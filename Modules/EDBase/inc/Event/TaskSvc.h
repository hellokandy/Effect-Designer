// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	TaskSvc.h
// File mark:   
// File summary:Task处理
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-11
// ----------------------------------------------------------------
#pragma once
#include "TaskFunT.h"

/// <summary>
///		处理Task队列的基类
/// </summary>
class ED_EXPORT Tasks
{
	typedef std::list<ITaskPtr>		t_lstTaskQueue;
public:
	Tasks();

public:
	bool AddNewTask(ITaskPtr pTask, bool bIsExec = true);// bIsExec为true，立即唤醒_RunTasksThread执行任务队列，立即退出
	bool ExecTask(bool bIsSync = false, DWORD dwTimeOut = INFINITE);// bIsSync为true时，表示同步等待_RunTasksThread执行任务队列的超时时间

protected:
	bool _InitTasksThread();
	DWORD _RunTasksThread();
	bool _UnInitTasksThread();

	bool _IsTaskRunning();

private:
	bool _DealWithMessage();
	void _GetNewTaskList();
	void _DealWithTaskList();
	bool _DestroyTasks();

public:
	t_lstTaskQueue								m_TaskWaitList;			///< 待处理的任务队列
	t_lstTaskQueue								m_TaskRunList;			///< 正在处理的任务队列
	DMLock										m_TaskLocker;			///< 任务锁
	DMLock*										m_pThreadLocker;        ///< 线程锁
	HANDLE										m_hTaskWaitEvent;		///< 等待处理任务事件
	HANDLE										m_hTaskFinishEvent;		///< 等待任务处理完成事件
	DWORD										m_dwThreadId;			///< 本线程的ID
	bool										m_bRunning;				///< 是否在运行中
	volatile bool								m_bExitTaskThread;	    ///< 是否退出
	DWORD										m_dwTimeOut;			///< 服务线程超时时间
};

/// <summary>
///		处理Tasks的线程
/// </summary>
class ED_EXPORT TasksThread: public Tasks
{
public:
	TasksThread();

public:
	bool InitTasks(DWORD dwTimeOut = INFINITE);
	bool RunTasks();
	bool StopTasks();

public:
	virtual void OnStart();

protected:
	static unsigned int WINAPI ThreadProc(LPVOID lp);

public:
	HANDLE										m_hThread;
};

/// <summary>
///		 处理Tasks对外包装接口
/// </summary>
class ED_EXPORT TaskSvc
{
public:
	TaskSvc();
	~TaskSvc();

public:
	bool InitTaskSvc(DWORD dwTimeOut = INFINITE);
	bool RunTaskSvc();
	bool StopTaskSvc();
	bool AddTask(ITask* pTask, bool bExec = true);
	bool ExecTask(bool bSync = false, DWORD dwTimeOut = INFINITE);

protected:
	TasksThread* 							 m_pTaskImpl;
};