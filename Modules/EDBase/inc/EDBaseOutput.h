// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	GPBaseOutput.h
// File mark:   
// File summary:EDBASE使用的导出文件
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-9
// ----------------------------------------------------------------
#pragma once

// DM
#include "DmMainOutput.h"

#ifdef _DEBUG
#pragma comment(lib,"DmMain_d.lib")
#else
#pragma comment(lib,"DmMain.lib")
#endif

using namespace DM;
#include <algorithm>
#include <map>
#include <vector>
#include <list>
#include <string>

// ----------------------------------------------------------------
// 导出宏处理
#ifdef	 ED_API_EXPORTS
#define  ED_EXPORT		 __declspec( dllexport )
#else
#define	 ED_EXPORT		 __declspec( dllimport )
#endif// GP_API_EXPORTS

// ----------------------------------------------------------------	
#include "GPMapT.h"
#include "GPVecT.h"
#include "CodeHelper.h"					///< 导出字符转换相关辅助
#include "UrlHelper.h"					///< URL相关辅助
#include "JSHelper.h"					///< 导出JS封装接口
#include "DumpReport.h"					///< 导出Dump上报接口
#include "TaskSvc.h"					///< 导出任务线程
#include "EventGlobal.h"				///< 导出分发事件接口
#include "TrayIconWnd.h"                ///< 导出托盘封装

//#include "IHttpReq.h"					///< 导出Http封装接口

// lzh 
#include "GPIniFile.h"