#include "EDBaseAfx.h"
#include "EventGlobal.h"
#include "EventEx.h"

DMLazyT<EventSetEx>        g_pEventSetEx;
void EventGlobal::initEvent(DWORD dwThreadId)
{
	g_pEventSetEx->InitEvent(dwThreadId);
}

void EventGlobal::uninitEvent(DWORD dwThreadId)
{
	g_pEventSetEx.destroy();
}

bool EventGlobal::existEvent(LPCSTR pszEventName)
{
	if (IsValidString(pszEventName)&&g_pEventSetEx) 
	{
		return g_pEventSetEx->ExistEvent(pszEventName);
	}
	return false;
}

void EventGlobal::removeEvent(LPCSTR pszEventName)
{
	if (IsValidString(pszEventName)&&g_pEventSetEx) 
	{
		g_pEventSetEx->RemoveEvent(pszEventName);
	}
}

void EventGlobal::removeEventIfThis(void* pThis)
{
	if (g_pEventSetEx)
	{
		g_pEventSetEx->RemoveEventIfThis(pThis);
	}
}

void EventGlobal::removeAllEvents(void)
{
	if (g_pEventSetEx)
	{
		g_pEventSetEx->RemoveAllEvents();
	}
}

bool EventGlobal::connectEvent(LPCSTR pszEventName, const GPSlot& slot, int group/* = -1*/)
{
	if (IsValidString(pszEventName)&&g_pEventSetEx) 
	{
		DMSmartPtrT<EventSlot> pSlot;pSlot.Attach(new EventSlot(slot));
		Event::Connection connect = g_pEventSetEx->ConnectEvent(pszEventName, *pSlot, group);
		return connect->Connected();
	}
	return false;
}

bool EventGlobal::connectUiEvent(LPCSTR pszEventName, const GPSlot& slot, int group/* = -1*/)
{
	if (IsValidString(pszEventName)&&g_pEventSetEx) 
	{
		Event::Connection connect = g_pEventSetEx->ConnectUiEvent(pszEventName, slot, group);
		return connect->Connected();
	}
	return false;
}

bool EventGlobal::connectAsyncEvent(LPCSTR pszEventName, const GPSlot& slot, int group/* = -1*/)
{
	if (IsValidString(pszEventName)) 
	{
		Event::Connection connect = g_pEventSetEx->ConnectAsyncEvent(pszEventName, slot, group);
		return connect->Connected();
	}
	return false;
}

void EventGlobal::fireEvent(LPCSTR pszEventName)
{
	if (IsValidString(pszEventName)&&g_pEventSetEx)
	{
		Bundle evt;
		return g_pEventSetEx->FireEvent(pszEventName, evt);
	}
}

void EventGlobal::fireEvent(LPCSTR pszEventName, Bundle& args)
{
	if (IsValidString(pszEventName)&&g_pEventSetEx) 
	{
		return g_pEventSetEx->FireEvent(pszEventName, args);
	}
}