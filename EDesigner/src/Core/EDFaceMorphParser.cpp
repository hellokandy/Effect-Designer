#include "StdAfx.h"
#include "EDFaceMorphParser.h"

// namespace DM
// {
EDFaceMorphParser::EDFaceMorphParser() :m_bAllowDiscardFrame(false), m_canvasSize(750, 1334), m_iTargetFPS(25)
{
}

EDFaceMorphParser::~EDFaceMorphParser()
{
}

DMCode EDFaceMorphParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (0 == _stricmp(pszAttribute, EDAttr::EDFaceMorphParserAttr::BOOL_allowDiscardFrame))
		{
			if (JsHandleValue.isBool())
			{
				m_bAllowDiscardFrame = JsHandleValue.toBool();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDFaceMorphParserAttr::SIZE_canvasSize))
		{
			if (JsHandleValue.isArray())
			{
				DMASSERT(2 == JsHandleValue.arraySize());
				if (2 <= JsHandleValue.arraySize())
				{
					m_canvasSize.cx = JsHandleValue[0].toInt();
					m_canvasSize.cy = JsHandleValue[1].toInt();
					iErr = DM_ECODE_OK;
				}
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDFaceMorphParserAttr::INT_targetFPS))
		{
			if (JsHandleValue.isInt())
			{
				m_iTargetFPS = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}

		if (!bLoadJSon)
			MarkDataDirty();
	} while (false);
	return iErr;
}

DMCode EDFaceMorphParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

DMCode EDFaceMorphParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	do
	{
		JSonHandler[EDAttr::EDFaceMorphParserAttr::BOOL_allowDiscardFrame].putBool(m_bAllowDiscardFrame);
		JSHandle JSon = JSonHandler[EDAttr::EDFaceMorphParserAttr::SIZE_canvasSize];
		JSon[0].putInt(m_canvasSize.cx);
		JSon[1].putInt(m_canvasSize.cy);
		JSonHandler[EDAttr::EDFaceMorphParserAttr::INT_targetFPS].putInt(m_iTargetFPS);
	} while (false);
	return DM_ECODE_OK;
}

int	EDFaceMorphParser::GetZPositionOrder()
{
	int iRet = -1;
	do 
	{
		EDJSonParser* pMakeUpParser = FindChildParserByClassName(EDMakeupsParser::GetClassNameW());
		if (!pMakeUpParser)
			break;
		EDJSonParser* pMakeUpNodeParser = pMakeUpParser->GetParser(GPS_FIRSTCHILD);
		if (pMakeUpNodeParser)
			iRet = pMakeUpNodeParser->GetZPositionOrder();
	} while (false);
	return iRet;
}

EDMakeupsParser::EDMakeupsParser()
{
}

EDMakeupsParser::~EDMakeupsParser()
{
}

DMCode EDMakeupsParser::InitJSonData(JSHandle &JSonHandler)
{
	DMCode iErr = DM_ECODE_OK;
	do
	{
		if (false == JSonHandler.isValid())
		{
			iErr = 200;
			break;
		}

		int cnt = JSonHandler.arraySize();
		for (int i = 0; i < cnt; ++i)
		{
			JSHandle& ItemHandle = JSonHandler[i];
			if (!ItemHandle.isValid())
			{
				continue;
			}

			iErr = ParseMemberJSObj(std::string(EDBASE::w2a(EDMakeupsNodeParser::GetClassNameW())).c_str(), ItemHandle, true);//解析makeupsnode节点
		}
 	} while (false);
	return iErr;
}

DMCode EDMakeupsParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

EDMakeupsNodeParser::EDMakeupsNodeParser()
{
	m_bEnable = true;
	m_dbImageScale = 1.0;
	m_iZPosition = -1;
	m_pRelativeResourceNodeParser = NULL;
	//m_strTag = L"Eye"; //default
}

EDMakeupsNodeParser::~EDMakeupsNodeParser()
{
}

DMCode EDMakeupsNodeParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (0 == _stricmp(pszAttribute, EDAttr::EDMakeupsNodeParserAttr::BOOL_enable))
		{
			if (JsHandleValue.isBool())
			{
				m_bEnable = JsHandleValue.toBool();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDMakeupsNodeParserAttr::STRING_imagePath))
		{
			if (JsHandleValue.isString())
			{
				m_strImagePath = std::wstring(EDBASE::a2w(JsHandleValue.toStringA())).c_str();//有中文字符 会出现编码出差  跟商汤的编码可能不一样
				//m_strImagePath = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDMakeupsNodeParserAttr::DOUBLE_imageScale))
		{
			if (JsHandleValue.isDouble())
			{
				m_dbImageScale = (float)JsHandleValue.toDouble();
				iErr = DM_ECODE_OK;
			}
			else if (JsHandleValue.isInt())
			{
				m_dbImageScale = (double)JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDMakeupsNodeParserAttr::STRING_name))
		{
			if (JsHandleValue.isString())
			{
				m_strName = std::wstring(EDBASE::a2w(JsHandleValue.toStringA())).c_str();//有中文字符 会出现编码出差  跟商汤的编码可能不一样
			//	m_strName = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDMakeupsNodeParserAttr::POINT_position))
		{
			if (JsHandleValue.isArray())
			{
				DMASSERT(2 == JsHandleValue.arraySize());
				if (2 <= JsHandleValue.arraySize())
				{
					m_ptPosition.x = JsHandleValue[0].toInt();
					m_ptPosition.y = JsHandleValue[1].toInt();
					iErr = DM_ECODE_OK;
				}
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDMakeupsNodeParserAttr::STRING_tag))
		{
			if (JsHandleValue.isString())
			{
				m_strTag = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDMakeupsNodeParserAttr::INT_zPosition))
		{
			if (JsHandleValue.isInt())
			{
				m_iZPosition = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else
		{//强制设置为OK 避免后面可能new出其他节点出来   因为已经不可能有子节点了
			DMASSERT_EXPR(false, L"makeups 有没有解析的节点");
			iErr = DM_ECODE_NOLOOP;
		}

		if (!bLoadJSon)
			MarkDataDirty();
	} while (false);
	return iErr;
}

DMCode EDMakeupsNodeParser::ParseJSNodeObjFinished()
{
	EDResourceNodeParser* pResourceNodeParser = NULL;
	if (m_strImagePath.GetLength() > 0)
	{
		EDResourceParser* pResourceParser = EDResourceParser::GetResourceParserIns(GetTopParentParser()); DMASSERT(pResourceParser);
		pResourceNodeParser = pResourceParser->InsertNewResourceNode(m_strImagePath, m_strName, TAGMAKEUPNODE);
		RelativeResourceNodeParser(pResourceNodeParser, true);
	}
	EDJSonParser::ParseJSNodeObjFinished();//调用父类
	return DM_ECODE_OK;
}

DMCode EDMakeupsNodeParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	int iSize = JSonHandler.arraySize();
	do
	{
		JSonHandler[iSize][EDAttr::EDMakeupsNodeParserAttr::BOOL_enable].putBool(m_bEnable);
		JSonHandler[iSize][EDAttr::EDResourceNodeParserAttr::STRING_imagePath].putString(std::string(EDBASE::w2a(m_strImagePath)).c_str());//有中文字符 会出现编码出差  跟商汤的编码可能不一样
	//	JSonHandler[iSize][EDAttr::EDMakeupsNodeParserAttr::STRING_imagePath].putStringW(m_strImagePath);
		JSonHandler[iSize][EDAttr::EDMakeupsNodeParserAttr::DOUBLE_imageScale].putDouble(m_dbImageScale);
		JSonHandler[iSize][EDAttr::EDResourceNodeParserAttr::STRING_name].putString(std::string(EDBASE::w2a(m_strName)).c_str());//有中文字符 会出现编码出差  跟商汤的编码可能不一样
		//JSonHandler[iSize][EDAttr::EDMakeupsNodeParserAttr::STRING_name].putStringW(m_strName);
		JSonHandler[iSize][EDAttr::EDMakeupsNodeParserAttr::POINT_position][0].putInt(m_ptPosition.x);
		JSonHandler[iSize][EDAttr::EDMakeupsNodeParserAttr::POINT_position][1].putInt(m_ptPosition.y);
		JSonHandler[iSize][EDAttr::EDMakeupsNodeParserAttr::STRING_tag].putStringW(m_strTag);
		JSonHandler[iSize][EDAttr::EDMakeupsNodeParserAttr::INT_zPosition].putInt(m_iZPosition);
	} while (false);
	return DM_ECODE_FAIL;
}

DMCode EDMakeupsNodeParser::RelativeResourceNodeParser(EDResourceNodeParser* pResourceNodeParser, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		if (!pResourceNodeParser)//空
		{
			if (m_pRelativeResourceNodeParser) //之前有绑定资源 现在资源解除绑定
			{
				m_pRelativeResourceNodeParser->BindCurResourceParserOwner(NULL);
				if (!bLoadJSon) MarkDataDirty();
			}
			m_pRelativeResourceNodeParser = NULL;
			m_strName = INVALIDEPARSERNAME;
			break;
		}

		if (pResourceNodeParser != pResourceNodeParser || m_strName != pResourceNodeParser->m_strName || m_strImagePath != pResourceNodeParser->m_strImagePath)
		{//资源绑定指针不一致  或者memberkey不相同  说明资源改变
			if (!bLoadJSon)
				MarkDataDirty();
		}

		m_strImagePath = pResourceNodeParser->m_strImagePath;
		m_strName = pResourceNodeParser->m_strName;
		m_pRelativeResourceNodeParser = pResourceNodeParser;
		m_pRelativeResourceNodeParser->BindCurResourceParserOwner(this);
	} while (false);

	IEDJSonParserOwner* pParserOwner = GetParserOwner();
	if (pParserOwner)
	{
		pParserOwner->RelativeResourceNodeParserChgNotify(this);
	}
	return iErr;
}

EDResourceNodeParser* EDMakeupsNodeParser::GetRelativeResourceNodeParser()
{
	return m_pRelativeResourceNodeParser;
}

void EDMakeupsNodeParser::SetParserItemRect(const CRect& rect)
{
	do 
	{
		if (!m_pRelativeResourceNodeParser || !m_pRelativeResourceNodeParser->m_pBmp)
		{
			DMASSERT(FALSE);
			break;
		}

		CArray<int> PointArray;
		PointArray.Add(rect.left);
		PointArray.Add(rect.top);
		SetJSonArrayAttributeInt(EDAttr::EDMakeupsNodeParserAttr::POINT_position, PointArray);
		SetJSonAttributeDouble(EDAttr::EDMakeupsNodeParserAttr::DOUBLE_imageScale, (double)rect.Height() / m_pRelativeResourceNodeParser->m_pBmp->GetHeight());
	} while (false);
}

CPoint EDMakeupsNodeParser::GetParserItemTopLeftPt()
{
	return m_ptPosition;
}

double EDMakeupsNodeParser::GetParserImageScale()
{
	return m_dbImageScale;
}

int EDMakeupsNodeParser::GetZPositionOrder()
{
	return m_iZPosition;
}

bool EDMakeupsNodeParser::SetZPositionOrder(int iZPostion)
{
	m_iZPosition = iZPostion;
	return true;
}

bool EDMakeupsNodeParser::IsParserEnable()
{
	return m_bEnable;
}

//}; //end namespace DM