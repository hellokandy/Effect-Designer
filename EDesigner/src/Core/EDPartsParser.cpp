#include "StdAfx.h"
#include "EDPartsParser.h"

EDPartsParser::EDPartsParser()
{
}

EDPartsParser::~EDPartsParser()
{
}

DMCode EDPartsParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];//"parts"
	return EDJSonParser::BuildJSonData(JSonChild);
}

DMCode EDPartsParser::ParseMemberJSObj(LPCSTR pszAttribute, JSHandle& JSonHandler, bool bLoadJSon)
{//名字是"tiezhi"强制转换成：“PartsNode” 然后里面会创建新的PartsNode类对象
	EDJSonParser* pLastInsertParser = m_Node.m_pLastChild;
	DMCode iErr = EDJSonParser::ParseMemberJSObj(std::string(EDBASE::w2a(EDPartsNodeParser::GetClassNameW())).c_str(), JSonHandler, bLoadJSon);
	if (DM_ECODE_OK == iErr && m_Node.m_pLastChild && m_Node.m_pLastChild != pLastInsertParser)
	{
		m_Node.m_pLastChild->SetJSonMemberKey(pszAttribute); ///设置回原来的名字
		EDPartsNodeParser* pPartsNode = dynamic_cast<EDPartsNodeParser*>(m_Node.m_pLastChild);
		if (pPartsNode)
		{
			pPartsNode->AddParserRelativeResource(pszAttribute);
		}
	}
	return iErr;
}

EDPartsNodeParser::EDPartsNodeParser()
{
	m_bAllowDiscardFrame	= false;
	m_iBlendMode			= (int)BlendMode_Normal;
	m_bEnable				= true;
	m_iFrameCount			= 1;
	m_iHeight				= DEFRESPNGHEIGHT;
	m_bHotlinkEnable		= false;
	m_iPositionRelationType	= 1;
	m_iPositionType			= 1;
	m_iPositionType2		= 0;
	m_iTargetFPS			= 20;
	m_iWidth				= DEFRESPNGWIDTH;
	m_iZPosition			= -1;
	m_dbImageScale			= -1.0;
	m_pRelativeResourceNodeParser = NULL;

	int pIndex1 = 74, pIndex2 = 77, pIndex3 = 46;
	CPSPointNode point1(pIndex1, g_refPoints[pIndex1].x, g_refPoints[pIndex1].y);
	CPSPointNode point2(pIndex2, g_refPoints[pIndex2].x, g_refPoints[pIndex2].y);
	CPSPointNode point3(pIndex3, g_refPoints[pIndex3].x, g_refPoints[pIndex3].y);

	m_psPosition.m_positionX.Add(point1);
	m_psPosition.m_positionX.Add(point2);

	m_psRotateCenter.m_position.Add(point3);
	m_scScale.m_scaleX.AddPointAVal(point1);
	m_scScale.m_scaleX.AddPointBVal(point2);
}

EDPartsNodeParser::~EDPartsNodeParser()
{
}

//CPosition->SetJSonAttribute
DMCode EDPartsNodeParser::CPosition::SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon)
{
	// 要把默认数据清掉
	if (m_positionX.GetCount() > 0)
		m_positionX.RemoveAll();

	if (m_positionY.GetCount() > 0)
		m_positionY.RemoveAll();

	std::vector<std::string> numbers = JsHandleValue.memberNames();
	if (!numbers.empty())
	{
		for (std::string& item : numbers)
		{
			if (0 == _stricmp(item.c_str(), EDAttr::EDPartsNodeParserAttr::EDPartsNodePosition::COMPOSITE_positionX))
			{
				JSHandle JsPsXValue = JsHandleValue[item.c_str()];
				for (int i = 0; i < JsPsXValue.arraySize(); i++)
				{
					JSHandle JsPsXNodeValue = JsPsXValue[i];
					m_positionX.Add(EDPartsNodeParser::CPSPointNode(JsPsXNodeValue["index"].toInt(), JsPsXNodeValue["x"].toDouble(), JsPsXNodeValue["y"].toDouble()));
				}
			}
			else if (0 == _stricmp(item.c_str(), EDAttr::EDPartsNodeParserAttr::EDPartsNodePosition::COMPOSITE_positionY))
			{
				JSHandle JsPsYValue = JsHandleValue[item.c_str()];
				for (int i = 0; i < JsPsYValue.arraySize(); i++)
				{
					JSHandle JsPsYNodeValue = JsPsYValue[i];
					m_positionY.Add(EDPartsNodeParser::CPSPointNode(JsPsYNodeValue["index"].toInt(), JsPsYNodeValue["x"].toDouble(), JsPsYNodeValue["y"].toDouble()));
				}
			}
			else
				DMASSERT_EXPR(FALSE, L"解析到postion不应该有的字段");
		}
	}
	return DM_ECODE_OK;
}

//CPosition->BuildMemberJsonData
DMCode EDPartsNodeParser::CPosition::BuildMemberJsonData(JSHandle &JSonHandler)
{
	JSHandle &JSonXHandler = JSonHandler[EDAttr::EDPartsNodeParserAttr::EDPartsNodePosition::COMPOSITE_positionX];
	if (m_positionX.IsEmpty())
	{
		JSObject obj(JSHandle::JsonArray);
		JSonXHandler.swap(obj);
	}
	else
	{
		for (size_t i = 0; i < m_positionX.GetCount(); i++)
		{
			JSonXHandler[i]["index"].putInt(m_positionX.GetAt(i).m_index);
			JSonXHandler[i]["x"].putDouble(m_positionX.GetAt(i).m_x);
			JSonXHandler[i]["y"].putDouble(m_positionX.GetAt(i).m_y);
		}
	}

	JSHandle &JSonYHandler = JSonHandler[EDAttr::EDPartsNodeParserAttr::EDPartsNodePosition::COMPOSITE_positionY];
	if (m_positionY.IsEmpty())
	{
		JSObject obj(JSHandle::JsonArray);
		JSonYHandler.swap(obj);
	}
	else
	{
		for (size_t i = 0; i < m_positionY.GetCount(); i++)
		{
			JSonYHandler[i]["index"].putInt(m_positionY.GetAt(i).m_index);
			JSonYHandler[i]["x"].putDouble(m_positionY.GetAt(i).m_x);
			JSonYHandler[i]["y"].putDouble(m_positionY.GetAt(i).m_y);
		}
	}	
	return DM_ECODE_OK;
}

//CRotateCenter->SetJSonAttribute
DMCode EDPartsNodeParser::CRotateCenter::SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon)
{
	if (m_position.GetCount() > 0)
		m_position.RemoveAll();

	for (int i = 0; i < JsHandleValue.arraySize(); i++)
	{
		JSHandle JsPsNodeValue = JsHandleValue[i];
		m_position.Add(EDPartsNodeParser::CPSPointNode(JsPsNodeValue["index"].toInt(), JsPsNodeValue["x"].toDouble(), JsPsNodeValue["y"].toDouble()));
	}
	return DM_ECODE_OK;
}

//CRotateCenter->BuildMemberJsonData
DMCode EDPartsNodeParser::CRotateCenter::BuildMemberJsonData(JSHandle &JSonHandler)
{
	if (m_position.IsEmpty())
	{
		JSObject obj(JSHandle::JsonArray);
		JSonHandler.swap(obj);
	}
	else
	{
		for (size_t i = 0; i < m_position.GetCount(); i++)
		{
			JSonHandler[i]["index"].putInt(m_position.GetAt(i).m_index);
			JSonHandler[i]["x"].putDouble(m_position.GetAt(i).m_x);
			JSonHandler[i]["y"].putDouble(m_position.GetAt(i).m_y);
		}
	}
	return DM_ECODE_OK;
}

//CScale::CScaleXY::CPointAB->SetJSonAttribute
DMCode EDPartsNodeParser::CScale::CScaleXY::CPointAB::SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon)
{
	if (JsHandleValue.arraySize() > 0)//初始化的那个默认值不需要了
		m_position.RemoveAll();

	for (int i = 0; i < JsHandleValue.arraySize(); i++)
	{
		JSHandle JsPsNodeValue = JsHandleValue[i];
		m_position.Add(EDPartsNodeParser::CPSPointNode(JsPsNodeValue["index"].toInt(), JsPsNodeValue["x"].toDouble(), JsPsNodeValue["y"].toDouble()));
	}
	return DM_ECODE_OK;
}

//CScale::CScaleXY::CPointAB->BuildMemberJsonData
DMCode EDPartsNodeParser::CScale::CScaleXY::CPointAB::BuildMemberJsonData(JSHandle &JSonHandler)
{
	if (m_position.IsEmpty())
	{
		JSObject obj(JSHandle::JsonArray);
		JSonHandler.swap(obj);
	}
	else
	{
		for (size_t i = 0; i < m_position.GetCount(); i++)
		{
			JSonHandler[i]["index"].putInt(m_position.GetAt(i).m_index);
			JSonHandler[i]["x"].putDouble(m_position.GetAt(i).m_x);
			JSonHandler[i]["y"].putDouble(m_position.GetAt(i).m_y);
		}
	}
	return DM_ECODE_OK;
}

//CScale::CScaleXY->SetJSonAttribute
DMCode EDPartsNodeParser::CScale::CScaleXY::SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon)
{
	std::vector<std::string> numbers = JsHandleValue.memberNames();
	if (!numbers.empty())
	{
		for (std::string& strKey : numbers)
		{
			if (0 == _stricmp(strKey.c_str(), EDAttr::EDPartsNodeParserAttr::EDPartsNodeScale::COMPOSITE_pointA))
			{
				m_pointA.SetJSonAttribute(JsHandleValue[strKey.c_str()], bLoadJSon);
			}
			else if (0 == _stricmp(strKey.c_str(), EDAttr::EDPartsNodeParserAttr::EDPartsNodeScale::COMPOSITE_pointB))
			{
				m_pointB.SetJSonAttribute(JsHandleValue[strKey.c_str()], bLoadJSon);
			}
		}
	}
	return DM_ECODE_OK;
}

//CScale::CScaleXY->BuildMemberJsonData
DMCode EDPartsNodeParser::CScale::CScaleXY::BuildMemberJsonData(JSHandle &JSonHandler)
{
	m_pointA.BuildMemberJsonData(JSonHandler[EDAttr::EDPartsNodeParserAttr::EDPartsNodeScale::COMPOSITE_pointA]);
	m_pointB.BuildMemberJsonData(JSonHandler[EDAttr::EDPartsNodeParserAttr::EDPartsNodeScale::COMPOSITE_pointB]);
	return DM_ECODE_OK;
}

//CScale->SetJSonAttribute
DMCode EDPartsNodeParser::CScale::SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon)
{
	std::vector<std::string> numbers = JsHandleValue.memberNames();
	if (!numbers.empty())
	{
		for (std::string& strKey : numbers)
		{
			if (0 == _stricmp(strKey.c_str(), EDAttr::EDPartsNodeParserAttr::EDPartsNodeScale::COMPOSITE_scaleX))
			{
				m_scaleX.SetJSonAttribute(JsHandleValue[strKey.c_str()], bLoadJSon);
			}
			else if (0 == _stricmp(strKey.c_str(), EDAttr::EDPartsNodeParserAttr::EDPartsNodeScale::COMPOSITE_scaleY))
			{
				m_scaleY.SetJSonAttribute(JsHandleValue[strKey.c_str()], bLoadJSon);
			}
		}
	}
	return DM_ECODE_OK;
}

//CScale->BuildMemberJsonData
DMCode EDPartsNodeParser::CScale::BuildMemberJsonData(JSHandle &JSonHandler)
{
	m_scaleX.BuildMemberJsonData(JSonHandler[EDAttr::EDPartsNodeParserAttr::EDPartsNodeScale::COMPOSITE_scaleX]);
	m_scaleY.BuildMemberJsonData(JSonHandler[EDAttr::EDPartsNodeParserAttr::EDPartsNodeScale::COMPOSITE_scaleY]);
	return DM_ECODE_OK;
}

DMCode EDPartsNodeParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::BOOL_allowDiscardFrame))
		{
			if (JsHandleValue.isBool())
			{
				m_bAllowDiscardFrame = JsHandleValue.toBool();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::INT_blendMode))
		{
			if (JsHandleValue.isInt())
			{
				m_iBlendMode = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::BOOL_enable))
		{
			if (JsHandleValue.isBool())
			{
				m_bEnable = JsHandleValue.toBool();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::INT_frameCount))
		{
			if (JsHandleValue.isInt())
			{
				m_iFrameCount = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::INT_height))
		{
			if (JsHandleValue.isInt())
			{
				m_iHeight = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::BOOL_hotlinkEnable))
		{
			if (JsHandleValue.isBool())
			{
				m_bHotlinkEnable = JsHandleValue.toBool();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::COMPOSITE_position))
		{
			iErr = m_psPosition.SetJSonAttribute(JsHandleValue, bLoadJSon);
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::INT_positionRelationType))
		{
			if (JsHandleValue.isInt())
			{
				m_iPositionRelationType = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::INT_positionType))
		{
			if (JsHandleValue.isInt())
			{
				m_iPositionType = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::INT_positionType2))
		{
			if (JsHandleValue.isInt())
			{
				m_iPositionType2 = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::COMPOSITE_rotateCenter))
		{
			iErr = m_psRotateCenter.SetJSonAttribute(JsHandleValue, bLoadJSon);
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::COMPOSITE_scale))
		{
			iErr = m_scScale.SetJSonAttribute(JsHandleValue, bLoadJSon);
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::COMPOSITE_stickerGroup))
		{
			std::vector<std::string> numbers = JsHandleValue.memberNames();
			if (!numbers.empty())
			{
				for (std::string& item : numbers)
				{
					if (0 == _stricmp(item.c_str(), EDAttr::EDPartsNodeParserAttr::EDPartsNodeStickerGroup::INT_groupSize))
					{
						m_stickerGroup.m_nGroupSize = JsHandleValue[item.c_str()].toInt();
						iErr = DM_ECODE_OK;
					}
					else if (0 == _stricmp(item.c_str(), EDAttr::EDPartsNodeParserAttr::EDPartsNodeStickerGroup::INT_index))
					{
						m_stickerGroup.m_nIndex = JsHandleValue[item.c_str()].toInt();
						iErr = DM_ECODE_OK;
					}
				}
			}			
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::INT_targetFPS))
		{
			if (JsHandleValue.isInt())
			{
				m_iTargetFPS = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}		
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::INT_width))
		{
			if (JsHandleValue.isInt())
			{
				m_iWidth = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDPartsNodeParserAttr::INT_zPosition))
		{
			if (JsHandleValue.isInt())
			{
				m_iZPosition = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else
		{//强制设置为OK 避免后面可能new出其他节点出来   因为已经不可能有子节点了
			CStringW StrExp;
			StrExp.Format(L"PartsNode 没有解析%S节点", pszAttribute);
			DMASSERT_EXPR(false, StrExp);
			iErr = DM_ECODE_NOLOOP;
		}
		if (!bLoadJSon)
			MarkDataDirty();
	} while (false);
	return iErr;
}

DMCode EDPartsNodeParser::BuildJSonData(JSHandle JSonHandler)
{
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

DMCode EDPartsNodeParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	do
	{
		JSonHandler[EDAttr::EDPartsNodeParserAttr::BOOL_allowDiscardFrame].putBool(m_bAllowDiscardFrame);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::INT_blendMode].putInt(m_iBlendMode);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::BOOL_enable].putBool(m_bEnable);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::INT_frameCount].putInt(m_iFrameCount);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::INT_height].putInt(m_iHeight);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::BOOL_hotlinkEnable].putBool(m_bHotlinkEnable);
		m_psPosition.BuildMemberJsonData(JSonHandler[EDAttr::EDPartsNodeParserAttr::COMPOSITE_position]);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::INT_positionRelationType].putInt(m_iPositionRelationType);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::INT_positionType].putInt(m_iPositionType);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::INT_positionType2].putInt(m_iPositionType2);
		m_psRotateCenter.BuildMemberJsonData(JSonHandler[EDAttr::EDPartsNodeParserAttr::COMPOSITE_rotateCenter]);
		m_scScale.BuildMemberJsonData(JSonHandler[EDAttr::EDPartsNodeParserAttr::COMPOSITE_scale]);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::COMPOSITE_stickerGroup][EDAttr::EDPartsNodeParserAttr::EDPartsNodeStickerGroup::INT_groupSize].putInt(m_stickerGroup.m_nGroupSize);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::COMPOSITE_stickerGroup][EDAttr::EDPartsNodeParserAttr::EDPartsNodeStickerGroup::INT_index].putInt(m_stickerGroup.m_nIndex);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::INT_targetFPS].putInt(m_iTargetFPS);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::INT_width].putInt(m_iWidth);
		JSonHandler[EDAttr::EDPartsNodeParserAttr::INT_zPosition].putInt(m_iZPosition);
	} while (false);
	return DM_ECODE_OK;
}

DMCode EDPartsNodeParser::AddParserRelativeResource(LPCSTR lpKeyVal)
{
	EDResourceParser* pResourceParser = EDResourceParser::GetResourceParserIns(GetTopParentParser()); DMASSERT(pResourceParser);
	if (pResourceParser)
	{
		EDResourceNodeParser* pResNodeParser = pResourceParser->InsertNewResourceNode(/*strimagePath*/std::wstring(EDBASE::a2w(lpKeyVal)).c_str(), std::wstring(EDBASE::a2w(lpKeyVal)).c_str(), TAG2DSTICKER);
		RelativeResourceNodeParser(pResNodeParser, true);
	}
	return DM_ECODE_OK;
}

DMCode EDPartsNodeParser::RelativeResourceNodeParser(EDResourceNodeParser* pResourceNodeParser, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (!pResourceNodeParser)
		{
			if (m_pRelativeResourceNodeParser) //之前有绑定资源 现在资源解除绑定
			{
				m_pRelativeResourceNodeParser->BindCurResourceParserOwner(NULL);
				if (!bLoadJSon) MarkDataDirty();
			}
			m_pRelativeResourceNodeParser = NULL; //reset
			m_strJSonMemberKey = std::string(EDBASE::w2a(INVALIDEPARSERNAME)).c_str();
			m_iFrameCount = 0;
			break;
		}

		if (m_pRelativeResourceNodeParser != pResourceNodeParser || 0 != _stricmp((LPCSTR)m_strJSonMemberKey, std::string(EDBASE::w2a(m_pRelativeResourceNodeParser->m_strName)).c_str()))
		{//资源绑定指针不一致  或者memberkey不相同  说明资源改变
			if (!bLoadJSon)
				MarkDataDirty();
		}
		m_pRelativeResourceNodeParser = pResourceNodeParser;
		m_iFrameCount = m_pRelativeResourceNodeParser->m_iFrameCount;
		m_strJSonMemberKey = std::string(EDBASE::w2a(m_pRelativeResourceNodeParser->m_strName)).c_str();
		m_pRelativeResourceNodeParser->BindCurResourceParserOwner(this);
		m_pRelativeResourceNodeParser->GetImgSize(m_iWidth, m_iHeight);
		iErr = DM_ECODE_OK;
	} while (false);

	IEDJSonParserOwner* pParserOwner = GetParserOwner();
	if (pParserOwner)
	{
		pParserOwner->RelativeResourceNodeParserChgNotify(this);
	}
	return iErr;
}

EDResourceNodeParser* EDPartsNodeParser::GetRelativeResourceNodeParser()
{
	return m_pRelativeResourceNodeParser;
}

void EDPartsNodeParser::SetParserItemRect(const CRect& rect)
{
	do
	{
		double scaleX = (double)rect.Width() / m_iWidth;
		double scaleY = (double)rect.Height() / m_iHeight;
		m_dbImageScale = std::min<double>(scaleX, scaleY);

	/*	if (m_psPosition.m_positionX.GetCount() < 1) //positionX 默认positionX的坐标
		{
			int pIndex1 = 74, pIndex2 = 77;
			CPSPointNode point1(pIndex1, g_refPoints[pIndex1].x, g_refPoints[pIndex1].y);
			CPSPointNode point2(pIndex2, g_refPoints[pIndex2].x, g_refPoints[pIndex2].y);

			m_psPosition.m_positionX.Add(point1);
			m_psPosition.m_positionX.Add(point2);
		} */

#define CHANGEPOINTARRAY(Array)\
		for (size_t i = 0; i < Array.GetCount(); ++i){\
			Array[i].m_x = (g_refPoints[Array[i].m_index].x - rect.left) / m_dbImageScale;\
			Array[i].m_y = (g_refPoints[Array[i].m_index].y - rect.top) / m_dbImageScale;\
		}

		CHANGEPOINTARRAY(m_psPosition.m_positionX);// position
		CHANGEPOINTARRAY(m_psPosition.m_positionY);
		CHANGEPOINTARRAY(m_psRotateCenter.m_position);// rotate center
		CHANGEPOINTARRAY(m_scScale.m_scaleX.m_pointA.m_position);//scale
		CHANGEPOINTARRAY(m_scScale.m_scaleX.m_pointB.m_position);
		CHANGEPOINTARRAY(m_scScale.m_scaleY.m_pointA.m_position);
		CHANGEPOINTARRAY(m_scScale.m_scaleY.m_pointB.m_position);

		MarkDataDirty(true);

// 		JSObject JsObjectPosition(JSObject::JsonObject); //objectValue
// 		JSHandle &JSonXHandler = JsObjectPosition[EDAttr::EDPartsNodeParserAttr::EDPartsNodePosition::COMPOSITE_positionX];
// 		if (m_psPosition.m_positionX.IsEmpty())
// 		{
// 			JSObject obj(JSHandle::JsonArray);
// 			JSonXHandler.swap(obj);
// 		}
// 		else
// 		{
// 			for (size_t i = 0; i < m_psPosition.m_positionX.GetCount(); i++)
// 			{
// 				JSonXHandler[i]["index"].putInt(m_psPosition.m_positionX.GetAt(i).m_index);
// 				JSonXHandler[i]["x"].putDouble((g_refPoints[m_psPosition.m_positionX[i].m_index].x - rect.left) / m_dbImageScale);
// 				JSonXHandler[i]["y"].putDouble((g_refPoints[m_psPosition.m_positionX[i].m_index].y - rect.top) / m_dbImageScale);
// 			}
// 		}
// 
// 		JSHandle &JSonYHandler = JsObjectPosition[EDAttr::EDPartsNodeParserAttr::EDPartsNodePosition::COMPOSITE_positionY];
// 		if (m_psPosition.m_positionY.IsEmpty())
// 		{
// 			JSObject obj(JSHandle::JsonArray);
// 			JSonYHandler.swap(obj);
// 		}
// 		else
// 		{
// 			for (size_t i = 0; i < m_psPosition.m_positionY.GetCount(); i++)
// 			{
// 				JSonYHandler[i]["index"].putInt(m_psPosition.m_positionY.GetAt(i).m_index);
// 				JSonYHandler[i]["x"].putDouble((g_refPoints[m_psPosition.m_positionY[i].m_index].x - rect.left) / m_dbImageScale);
// 				JSonYHandler[i]["y"].putDouble((g_refPoints[m_psPosition.m_positionX[i].m_index].y - rect.top) / m_dbImageScale);
// 			}
// 		}
// 		DMCode iErr = SetJSonAttribute(EDAttr::EDPartsNodeParserAttr::COMPOSITE_position, JsObjectPosition, false);//设置postion
//		DMASSERT(iErr == DM_ECODE_OK);
	} while (false);
}

CPoint EDPartsNodeParser::GetParserItemTopLeftPt()
{
	if (m_scScale.m_scaleX.m_pointA.m_position.GetCount() == 0)
	{
		DMASSERT(FALSE);
		return CPoint(0, 0);
	}
	double scale = GetParserImageScale();
	CPSPointNode pointA = m_scScale.m_scaleX.m_pointA.m_position[0];
	int x = (int)(g_refPoints[pointA.m_index].x - pointA.m_x * scale);
	int y = (int)(g_refPoints[pointA.m_index].y - pointA.m_y * scale);
	return CPoint(x, y);
}

void EDPartsNodeParser::GetReferencePoint(CPoint& pt1, CPoint& pt2)
{
	pt1 = { -1, -1 }; pt2 = { -1, -1 };
	if (m_psPosition.m_positionX.GetCount() > 0)
	{
		int index = m_psPosition.m_positionX[0].m_index;
		if (index < countof(g_refPoints))
		{
			pt1.x = static_cast<LONG>(g_refPoints[index].x);
			pt1.y = static_cast<LONG>(g_refPoints[index].y);
		}
	}
	if (m_psPosition.m_positionX.GetCount() > 1)
	{
		int index = m_psPosition.m_positionX[1].m_index;
		if (index < countof(g_refPoints))
		{
			pt2.x = static_cast<LONG>(g_refPoints[index].x);
			pt2.y = static_cast<LONG>(g_refPoints[index].y);
		}
	}
}

void EDPartsNodeParser::GetReferencePointIndex(int& index1, int& index2)
{
	index1 = index2 = - 1;
	if (m_psPosition.m_positionX.GetCount() > 0)
	{
		index1 = m_psPosition.m_positionX[0].m_index;
	}
	if (m_psPosition.m_positionX.GetCount() > 1)
	{
		index2 = m_psPosition.m_positionX[1].m_index;
	}
}

void EDPartsNodeParser::SetReferencePoint(int iIndex1, int iIndex2)
{
	if (iIndex1 > -1 && iIndex1 < countof(g_refPoints))
	{
		if (m_psPosition.m_positionX.GetCount() > 0)
		{
			m_psPosition.m_positionX[0].m_index = iIndex1;
			//m_psPosition.m_positionX[0].m_x = g_refPoints[iIndex1].x;
			//m_psPosition.m_positionX[0].m_y = g_refPoints[iIndex1].y;
		}
		else
		{
			CPSPointNode point1(iIndex1, g_refPoints[iIndex1].x, g_refPoints[iIndex1].y);
			m_psPosition.m_positionX.Add(point1);
		}
	}
	else
		DMASSERT(FALSE);

	if (iIndex2 == -1) //删除
	{
		if (m_psPosition.m_positionX.GetCount() > 1)
		{
			m_psPosition.m_positionX.RemoveAt(1);
		}
	}
	else if (iIndex2 >= 0 && iIndex2 < countof(g_refPoints))
	{
		if (m_psPosition.m_positionX.GetCount() > 1)
		{
			m_psPosition.m_positionX[1].m_index = iIndex2;
			//m_psPosition.m_positionX[1].m_x = g_refPoints[iIndex2].x;
			//m_psPosition.m_positionX[1].m_y = g_refPoints[iIndex2].y;
		}
		else
		{
			CPSPointNode point2(iIndex2, g_refPoints[iIndex2].x, g_refPoints[iIndex2].y);
			m_psPosition.m_positionX.Add(point2);
		}
	}
	else
		DMASSERT(FALSE);
	MarkDataDirty(true);
}

void EDPartsNodeParser::SetScalePoint(int iIndex1, int iIndex2)
{
	if (iIndex1 > -1 && iIndex1 < countof(g_refPoints))
	{
		if (m_scScale.m_scaleX.m_pointA.m_position.GetCount() > 0)
		{
			m_scScale.m_scaleX.m_pointA.m_position[0].m_index = iIndex1;
// 			m_scScale.m_scaleX.m_pointA.m_position[0].m_x = g_refPoints[iIndex1].x;
// 			m_scScale.m_scaleX.m_pointA.m_position[0].m_y = g_refPoints[iIndex1].y;
		}
		else
		{
			DMASSERT(FALSE);
// 			CPSPointNode point1(iIndex1, g_refPoints[iIndex1].x, g_refPoints[iIndex1].y);
// 			m_scScale.m_scaleX.m_pointA.m_position.Add(point1);
		}
	}
	else
		DMASSERT(FALSE);

	if (iIndex2 >= 0 && iIndex2 < countof(g_refPoints))
	{
		if (m_scScale.m_scaleX.m_pointB.m_position.GetCount() > 0)
		{
			m_scScale.m_scaleX.m_pointB.m_position[0].m_index = iIndex2;
// 			m_scScale.m_scaleX.m_pointB.m_position[0].m_x = g_refPoints[iIndex2].x;
// 			m_scScale.m_scaleX.m_pointB.m_position[0].m_y = g_refPoints[iIndex2].y;
		}
		else
		{
			DMASSERT(FALSE);
			//CPSPointNode point1(iIndex1, g_refPoints[iIndex2].x, g_refPoints[iIndex2].y);
			//m_scScale.m_scaleX.m_pointB.m_position.Add(point1);
		}
	}
	else
		DMASSERT(FALSE);
	MarkDataDirty(true);
}

void EDPartsNodeParser::SetRotateCenterPoint(int iIndex)
{
	if (iIndex > -1 && iIndex < countof(g_refPoints))
	{
		if (m_psRotateCenter.m_position.GetCount() > 0)
		{
			m_psRotateCenter.m_position[0].m_index = iIndex;
// 			m_psRotateCenter.m_position[0].m_x = g_refPoints[iIndex].x;
// 			m_psRotateCenter.m_position[0].m_y = g_refPoints[iIndex].y;
		}
		else
		{
			CPSPointNode point1(iIndex, g_refPoints[iIndex].x, g_refPoints[iIndex].y);
			m_psRotateCenter.m_position.Add(point1);
		}
		MarkDataDirty(true);
	}
	else
		DMASSERT(FALSE);
}

double EDPartsNodeParser::GetParserImageScale()
{
	if (m_dbImageScale < 0)
	{
		ResetParserImageScale();
	}
	return m_dbImageScale;
}

void EDPartsNodeParser::ResetParserImageScale()
{
	if (m_scScale.m_scaleX.m_pointA.m_position.GetCount() == 0 || m_scScale.m_scaleX.m_pointB.m_position.GetCount() == 0)
	{
		DMASSERT(FALSE);
		return;
	}
	// 这里面有个假设，scalex的两个点不能相同
	CPSPointNode pointA = m_scScale.m_scaleX.m_pointA.m_position[0];
	CPSPointNode pointB = m_scScale.m_scaleX.m_pointB.m_position[0];

	double scaleX = (g_refPoints[pointA.m_index].x - g_refPoints[pointB.m_index].x) / (pointA.m_x - pointB.m_x);
	double scaleY = (g_refPoints[pointA.m_index].y - g_refPoints[pointB.m_index].y) / (pointA.m_y - pointB.m_y);
	m_dbImageScale =  std::min<double>(scaleX, scaleY);
}

int EDPartsNodeParser::GetZPositionOrder()
{
	return m_iZPosition;
}

bool EDPartsNodeParser::SetZPositionOrder(int iZPostion)
{
	m_iZPosition = iZPostion;
	return true;
}

bool EDPartsNodeParser::IsParserEnable()
{
	return m_bEnable;
}

DM::DMCode EDPartsNodeParser::GetRelativeResourceImgSize(INT& uiWidth, INT& uiHeight)
{
	uiWidth = m_iWidth;
	uiHeight = m_iHeight;
	return DM_ECODE_OK;
}
