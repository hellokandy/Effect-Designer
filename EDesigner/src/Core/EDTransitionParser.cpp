#include "StdAfx.h"
#include "EDTransitionParser.h"

// namespace DM
// {
EDTransitionsParser::EDTransitionsParser()
{
}

EDTransitionsParser::~EDTransitionsParser()
{
}

DMCode EDTransitionsParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];//"transitions"
	return EDJSonParser::BuildJSonData(JSonChild);
}

DMCode EDTransitionsParser::ParseMemberJSObj(LPCSTR pszAttribute, JSHandle& JSonHandler, bool bLoadJSon)
{//名字是"transition1"强制转换成：“TransitionNode” 然后里面会创建新的PartsNode类对象
	EDJSonParser* pLastInsertParser = m_Node.m_pLastChild;
	DMCode iErr = EDJSonParser::ParseMemberJSObj(std::string(EDBASE::w2a(EDTransitionsNodeParser::GetClassNameW())).c_str(), JSonHandler, bLoadJSon);
	if (DM_ECODE_OK == iErr && m_Node.m_pLastChild && m_Node.m_pLastChild != pLastInsertParser)
	{
		m_Node.m_pLastChild->SetJSonMemberKey(pszAttribute); ///设置回原来的名字
	}
	return iErr;
}

EDTransitionsNodeParser::EDTransitionsNodeParser() : m_bRandomShow(false)
{
}

EDTransitionsNodeParser::~EDTransitionsNodeParser()
{
}

DMCode EDTransitionsNodeParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (0 == _stricmp(pszAttribute, EDAttr::EDTransitionsNodeParserAttr::BOOL_randomShow))
		{
			if (JsHandleValue.isBool())
			{
				m_bRandomShow = JsHandleValue.toBool();
				iErr = DM_ECODE_OK;
			}
		}
		if (!bLoadJSon)
			MarkDataDirty();
	} while (false);
	return iErr;
}

DMCode EDTransitionsNodeParser::BuildJSonData(JSHandle JSonHandler)
{
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

DMCode EDTransitionsNodeParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	do
	{
		JSonHandler[EDAttr::EDTransitionsNodeParserAttr::BOOL_randomShow].putBool(m_bRandomShow);
	} while (false);
	return DM_ECODE_OK;
}

EDConditionsParser::EDConditionsParser()
{
}

EDConditionsParser::~EDConditionsParser()
{
}

DMCode EDConditionsParser::InitJSonData(JSHandle &JSonHandler)
{
	DMCode iErr = DM_ECODE_OK;
	do
	{
		if (false == JSonHandler.isValid())
		{
			iErr = 200;
			break;
		}

		int cnt = JSonHandler.arraySize();
		for (int i = 0; i < cnt; ++i)
		{
			JSHandle& ItemHandle = JSonHandler[i];
			if (!ItemHandle.isValid())
			{
				continue;
			}

			iErr = ParseMemberJSObj(std::string(EDBASE::w2a(EDConditionsNodeParser::GetClassNameW())).c_str(), ItemHandle, true);//解析Conditionsnode节点
		}
	} while (false);
	return iErr;
}

DMCode EDConditionsParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

EDConditionsNodeParser::EDConditionsNodeParser()
{
	m_strTriggers = L"H.face.appear"; //default action type
	m_bTreeItemCollapsed = false;
}

EDConditionsNodeParser::~EDConditionsNodeParser()
{
}

DMCode EDConditionsNodeParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (0 == _stricmp(pszAttribute, EDAttr::EDConditionsNodeParserAttr::STRING_preState))
		{
			if (JsHandleValue.isString())
			{
				m_strPreState = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDConditionsNodeParserAttr::STRING_triggers))
		{
			if (JsHandleValue.isString())
			{
				m_strTriggers = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}		
		else
		{//强制设置为OK 避免后面可能new出其他节点出来   因为已经不可能有子节点了
			DMASSERT_EXPR(false, L"Conditions 有没有解析的节点");
			iErr = DM_ECODE_NOLOOP;
		}

		if (!bLoadJSon)
			MarkDataDirty();
	} while (false);
	return iErr;
}

DMCode EDConditionsNodeParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	int iSize = JSonHandler.arraySize();
	do
	{
		JSonHandler[iSize][EDAttr::EDConditionsNodeParserAttr::STRING_preState].putStringW(m_strPreState);
		JSonHandler[iSize][EDAttr::EDConditionsNodeParserAttr::STRING_triggers].putStringW(m_strTriggers);
	} while (false);
	return DM_ECODE_FAIL;
}

EDTargetsParser::EDTargetsParser()
{
}

EDTargetsParser::~EDTargetsParser()
{
}

DMCode EDTargetsParser::InitJSonData(JSHandle &JSonHandler)
{
	DMCode iErr = DM_ECODE_OK;
	do
	{
		if (false == JSonHandler.isValid())
		{
			iErr = 200;
			break;
		}

		int cnt = JSonHandler.arraySize();
		for (int i = 0; i < cnt; ++i)
		{
			JSHandle& ItemHandle = JSonHandler[i];
			if (!ItemHandle.isValid())
			{
				continue;
			}

			iErr = ParseMemberJSObj(std::string(EDBASE::w2a(EDTargetsNodeParser::GetClassNameW())).c_str(), ItemHandle, true);//解析Targetsnode节点
		}
	} while (false);
	return iErr;
}

DMCode EDTargetsParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

EDTargetsNodeParser::EDTargetsNodeParser()
{
	m_iDelay 		= 0;
	m_iFadingFrame 	= 0;
	m_iLastingFrame = 0;
	m_iLoop 		= 0;
	m_strTargetState = L"playing"; //default data
	m_bTreeItemCollapsed = false;
}

EDTargetsNodeParser::~EDTargetsNodeParser()
{
}

DMCode EDTargetsNodeParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (0 == _stricmp(pszAttribute, EDAttr::EDTargetsNodeParserAttr::INT_delay))
		{
			if (JsHandleValue.isInt())
			{
				m_iDelay = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDTargetsNodeParserAttr::INT_fadingFrame))
		{
			if (JsHandleValue.isInt())
			{
				m_iFadingFrame = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDTargetsNodeParserAttr::INT_lastingFrame))
		{
			if (JsHandleValue.isInt())
			{
				m_iLastingFrame = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDTargetsNodeParserAttr::INT_loop))
		{
			if (JsHandleValue.isInt())
			{
				m_iLoop = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDTargetsNodeParserAttr::STRING_targetPart))
		{
			if (JsHandleValue.isString())
			{
				m_strTargetPart = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDTargetsNodeParserAttr::STRING_targetState))
		{
			if (JsHandleValue.isString())
			{
				m_strTargetState = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else
		{//强制设置为OK 避免后面可能new出其他节点出来   因为已经不可能有子节点了
			DMASSERT_EXPR(false, L"Targets 有没有解析的节点");
			iErr = DM_ECODE_NOLOOP;
		}

		if (!bLoadJSon)
			MarkDataDirty();
	} while (false);
	return iErr;
}

DMCode EDTargetsNodeParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	int iSize = JSonHandler.arraySize();
	do
	{
		JSonHandler[iSize][EDAttr::EDTargetsNodeParserAttr::INT_delay].putInt(m_iDelay);
		JSonHandler[iSize][EDAttr::EDTargetsNodeParserAttr::INT_fadingFrame].putInt(m_iFadingFrame);
		JSonHandler[iSize][EDAttr::EDTargetsNodeParserAttr::INT_lastingFrame].putInt(m_iLastingFrame);
		JSonHandler[iSize][EDAttr::EDTargetsNodeParserAttr::INT_loop].putInt(m_iLoop);
		JSonHandler[iSize][EDAttr::EDTargetsNodeParserAttr::STRING_targetPart].putStringW(m_strTargetPart);
		JSonHandler[iSize][EDAttr::EDTargetsNodeParserAttr::STRING_targetState].putStringW(m_strTargetState);
	} while (false);
	return DM_ECODE_FAIL;
}

//}; //end namespace DM