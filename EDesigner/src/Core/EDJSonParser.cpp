#include "StdAfx.h"
#include "EDJSonParser.h"

JSObject EDJSonParser::m_JSobject(JSObject::JsonObject);
EDJSonParser::EDJSonParser() :m_bDataDirtyMark(false), m_hTreeBindItem(NULL)
{

}

EDJSonParser::~EDJSonParser()
{

}

DMCode EDJSonParser::SetJSonMemberKey(LPCSTR lpKeyVal)
{
	m_strJSonMemberKey = lpKeyVal;
	return DM_ECODE_OK;
}

void EDJSonParser::MarkDataDirtyRecursive(bool bDirty)
{
	m_bDataDirtyMark = bDirty;
	EDJSonParser* pChild = GetParser(GPS_FIRSTCHILD);
	while (pChild)
	{// 递归子节点
		pChild->MarkDataDirtyRecursive(bDirty);
		pChild = pChild->GetParser(GPS_NEXTSIBLING);
	}
}

bool EDJSonParser::RecursiveCheckIsDataDirty()
{
	if (m_bDataDirtyMark)
		return true;
	EDJSonParser* pChild = GetParser(GPS_FIRSTCHILD);
	while (pChild)
	{// 递归子节点
		if (pChild->RecursiveCheckIsDataDirty())
			return true;
		pChild = pChild->GetParser(GPS_NEXTSIBLING);
	}
	return false;
}

void EDJSonParser::MarkDataDirty(bool bDirty /*= true*/)
{
	m_bDataDirtyMark = bDirty;
	if (m_bDataDirtyMark && GetParserOwner())
	{
		GetParserOwner()->JSonParserDataDirtyNodtify(this);
	}
}
///< 外部设置Int类型数据回json文档 会设置脏数据
DMCode EDJSonParser::SetJSonAttributeInt(LPCSTR pszAttribute, int iVal)
{
	JSObject JsObject(JSObject::JsonInt);
	JsObject.putInt(iVal);
	DMCode iErr = SetJSonAttribute(pszAttribute, JsObject, false);
	DMASSERT(iErr == DM_ECODE_OK);
	return iErr;
}
///< 外部设置bool类型数据回json文档 会设置脏数据
DMCode EDJSonParser::SetJSonAttributeBool(LPCSTR pszAttribute, bool bVale)
{
	JSObject JsObject(JSObject::JsonBool);
	JsObject.putBool(bVale);
	DMCode iErr = SetJSonAttribute(pszAttribute, JsObject, false);
	DMASSERT(iErr == DM_ECODE_OK);
	return iErr;
}
///< 外部设置double类型数据回json文档 会设置脏数据
DMCode EDJSonParser::SetJSonAttributeDouble(LPCSTR pszAttribute, double dbVale)
{
	JSObject JsObject(JSObject::JsonDouble);
	JsObject.putDouble(dbVale);
	DMCode iErr = SetJSonAttribute(pszAttribute, JsObject, false);
	DMASSERT(iErr == DM_ECODE_OK);
	return iErr;
}
///< 外部设置string类型数据回json文档 会设置脏数据
DMCode EDJSonParser::SetJSonAttributeString(LPCSTR pszAttribute, LPCSTR lpVale)
{
	JSObject JsObject(JSObject::JsonString);
	JsObject.putString(lpVale);
	DMCode iErr = SetJSonAttribute(pszAttribute, JsObject, false);
	DMASSERT(iErr == DM_ECODE_OK);
	return iErr;
}
///< 外部设置string类型数据回json文档 会设置脏数据
DMCode EDJSonParser::SetJSonAttributeString(LPCSTR pszAttribute, LPCWSTR lpVale)
{
	JSObject JsObject(JSObject::JsonString);
	JsObject.putStringW(lpVale);
	DMCode iErr = SetJSonAttribute(pszAttribute, JsObject, false);
	DMASSERT(iErr == DM_ECODE_OK);
	return iErr;
}

///< 外部设置member中的int类型数据回json文档 会设置脏数据 "stickerGroup":{"groupSize":6, "index":0}	JSonHandler[EDAttr::EDPartsNodeParserAttr::COMPOSITE_stickerGroup][EDAttr::EDPartsNodeParserAttr::EDPartsNodeStickerGroup::INT_groupSize].putInt(m_stickerGroup.m_nGroupSize);
DMCode EDJSonParser::SetJSonMemberAttributeInt(LPCSTR pszMemberName, LPCSTR pszAttribute, int iVale)
{
	JSObject JsObjectItem(JSObject::JsonObject); //objectValue
	JsObjectItem[pszAttribute].putInt(iVale);
	DMCode iErr = SetJSonAttribute(pszMemberName, JsObjectItem, false);
	DMASSERT(iErr == DM_ECODE_OK);
	return iErr;
}

///< 外部设置Array的int类型数据回json文档 会设置脏数据
DMCode EDJSonParser::SetJSonArrayAttributeInt(LPCSTR pszAttribute, const CArray<int>& array)
{
	JSObject JsObjectArray(JSObject::JsonArray); //objectValue
	if (!array.IsEmpty())
	{
		for (size_t i = 0; i < array.GetCount(); i++)
		{
			JsObjectArray[i].putInt(array[i]);
		}
	}
	else //下面的可能不需要
	{
		JSObject obj(JSHandle::JsonArray);
		JsObjectArray.swap(obj);
	}
	DMCode iErr = SetJSonAttribute(pszAttribute, JsObjectArray, false);
	DMASSERT(iErr == DM_ECODE_OK);
	return iErr;
}

DMCode EDJSonParser::LoadJSonFile(CStringW strJSonPath)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		DWORD dwSize = GetFileSizeW(strJSonPath);
		if (dwSize <= 0)
		{
			break;
		}

		DMBufT<byte> pBuf;
		pBuf.Allocate(dwSize);
		DWORD dwRead;
		if (!GetFileBufW(strJSonPath, (void**)&pBuf, dwSize, dwRead))
			break;

		std::string content;
		content.assign((const char *)pBuf.get(), dwRead);
		if (m_JSobject.jsonParse(content))
		{
			iErr = InitJSonData(m_JSobject);
			ParseJSNodeObjFinished();
		}
	} while (false);
	return iErr;
}

DMCode EDJSonParser::InitJSonData(JSHandle &JSonHandler)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		RemoveAll();//remove unparser json node
		if (!JSonHandler.isValid())
		{
			break;
		}

		iErr = EDBase::InitJSonData(JSonHandler);

	} while (false);
	return iErr;
}

DMCode EDJSonParser::ParseMemberJSObj(LPCSTR pszAttribute, JSHandle& JSonHandler, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_OK;
	do
	{
		if (DM_ECODE_OK == (iErr = SetJSonAttribute(pszAttribute, JSonHandler, bLoadJSon)))
			break;
						
		EDJSonParser *pChild = NULL;
		if (g_pDMApp && DM_ECODE_NOLOOP != iErr)
		{
			CStringW strClassName;
			strClassName.Format(L"%S", pszAttribute);
			g_pDMApp->CreateRegObj((void**)&pChild, strClassName, DMREG_Attribute);
		}

		if (pChild)
		{
			pChild->SetJSonMemberKey(pszAttribute);
			InsertChildParser(pChild);
			IEDJSonParserOwner* pParserOwner = GetParserOwner();
			if (pParserOwner)
			{
				pParserOwner->NewJsonParserCreatedNotify(pChild);
			}
			pChild->InitJSonData(JSonHandler);
			iErr = pChild->ParseJSNodeObjFinished();
		}
		else
		{
			AddKey(pszAttribute, JSonHandler);//没有解析的字段  保存在json里面  下次buil的时候直接使用  避免没有解析到的节点丢失
		}			
	} while (false);
	return iErr;
}

DMCode EDJSonParser::ParseJSNodeObjFinished()
{
	if (GetParserOwner())
	{
		GetParserOwner()->ParseJSNodeObjFinished(this);
	}
	return DM_ECODE_OK;
}

static bool StrSaveToFile(const std::string& str, LPCWSTR lpFileName)
{
	std::ofstream ofs(EDBASE::w2a(lpFileName), std::ifstream::out);
	if (ofs.is_open())
	{
		ofs << str;
		ofs.close();
		return true;
	}
	ofs.close();
	return false;
}

DMCode EDJSonParser::BuildJSonFile(CStringW strJSonPath)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		std::string jsoutStr;
		JSObject jsobject(JSObject::JsonObject);
		BuildJSonData(jsobject);

		if (jsobject.jsonWrite(&jsoutStr))
		{
			StrSaveToFile(jsoutStr, strJSonPath);
		}

	} while (false);
	return iErr;

}

DMCode EDJSonParser::BuildJSonData(JSHandle JSonHandler)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		BuildMemberJsonData(JSonHandler);
		BuildUnParserNode(JSonHandler);

		EDJSonParser* pChild = GetParser(GPS_FIRSTCHILD);
		while (pChild)
		{// 递归子节点
			pChild->BuildJSonData(JSonHandler);
			pChild = pChild->GetParser(GPS_NEXTSIBLING);
		}
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDJSonParser::BuildUnParserNode(JSHandle &JSonHandler)
{
	POSITION poscur = m_Map.GetStartPosition();
	while (poscur)
	{
		DM::CMap<CStringA, JSHandle>::CPair *pcur = m_Map.GetNext(poscur);
		JSonHandler[pcur->m_key] = pcur->m_value;
	}
	return DM_ECODE_OK;
}

DMCode EDJSonParser::InsertChildParser(EDJSonParser* pNewChild, EDJSonParser* pInsertAfter /*= JSPARSER_LAST*/)		///< 插入子节点
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (NULL == pNewChild)
		{
			break;
		}

		pNewChild->m_Node.m_pParent = this;
		pNewChild->m_Node.m_pPrevSibling = pNewChild->m_Node.m_pNextSibling = NULL;
		pNewChild->m_Node.m_pOwner = this->m_Node.m_pOwner;

		if (pInsertAfter == m_Node.m_pLastChild)// 如果last为最后一个控件
		{
			pInsertAfter = JSPARSER_LAST;
		}

		if (JSPARSER_LAST == pInsertAfter)
		{
			pNewChild->m_Node.m_pPrevSibling = m_Node.m_pLastChild;
			if (m_Node.m_pLastChild)
			{
				m_Node.m_pLastChild->m_Node.m_pNextSibling = pNewChild;
			}
			else // 插入到最前面
			{
				m_Node.m_pFirstChild = pNewChild;
			}
			m_Node.m_pLastChild = pNewChild;
		}
		else if (pInsertAfter == JSPARSER_FIRST)
		{
			pNewChild->m_Node.m_pNextSibling = m_Node.m_pFirstChild;
			if (m_Node.m_pFirstChild)// 插入到最后面
			{
				m_Node.m_pFirstChild->m_Node.m_pPrevSibling = pNewChild;
			}
			else
			{
				m_Node.m_pLastChild = pNewChild;
			}
			m_Node.m_pFirstChild = pNewChild;
		}
		else
		{
			// 插入到中间-意味着前后都有窗口了！
			if (pInsertAfter->m_Node.m_pParent != this)// 被插入的子窗口的父窗口不是调用者！
			{
				break;
			}
			if (NULL == m_Node.m_pFirstChild || NULL == m_Node.m_pLastChild)
			{
				break;
			}

			EDJSonParser *pNext = pInsertAfter->m_Node.m_pNextSibling;
			if (NULL == pNext)
			{
				break;
			}

			pInsertAfter->m_Node.m_pNextSibling = pNewChild;
			pNewChild->m_Node.m_pPrevSibling = pInsertAfter;
			pNewChild->m_Node.m_pNextSibling = pNext;
			pNext->m_Node.m_pPrevSibling = pNewChild;
		}
		m_Node.m_nChildrenCount++;
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDJSonParser::MoveParserItemToNewPos(EDJSonParser* pInsertAfter /*= JSPARSER_LAST*/)
{
	EDJSonParser* pParentParser = GetParser(GPS_PARENT); DMASSERT(pParentParser);
	if (pParentParser)
	{
		pParentParser->RemoveChildParser(this);
		pParentParser->InsertChildParser(this, pInsertAfter);
		MarkDataDirty(true);
		return DM_ECODE_OK;
	}
	return DM_ECODE_FAIL;
}

EDJSonParserPtr EDJSonParser::CreateChildParser(CStringW strChildClassName, EDJSonParser* pInsertAfter)
{
	EDJSonParser *pChild = NULL;
	do
	{
		if (g_pDMApp)
		{
			g_pDMApp->CreateRegObj((void**)&pChild, strChildClassName, DMREG_Attribute);
		}
		if (pChild)
		{
			if (DM_ECODE_OK == InsertChildParser(pChild, pInsertAfter))
				MarkDataDirty(true);
		}
	} while (false);
	return pChild;
}

EDJSonParser* EDJSonParser::GetParser(int iCode)
{
	EDJSonParser *pParser = NULL;
	switch (iCode)
	{
	case GDW_FIRSTCHILD:  pParser = m_Node.m_pFirstChild;	break;
	case GDW_LASTCHILD:	  pParser = m_Node.m_pLastChild;	break;
	case GDW_PREVSIBLING: pParser = m_Node.m_pPrevSibling;	break;
	case GDW_NEXTSIBLING: pParser = m_Node.m_pNextSibling;	break;
	case GDW_PARENT:      pParser = m_Node.m_pParent;		break;
	}
	return pParser;
}

IEDJSonParserOwner* EDJSonParser::GetParserOwner()
{
	return m_Node.m_pOwner;
}

DMCode EDJSonParser::RelativeResourceNodeParser(EDResourceNodeParser* pResourceNodeParser, bool bLoadJSon)
{
	return DM_ECODE_NOTIMPL;
}

DMCode EDJSonParser::GetRelativeResourceImgSize(INT& uiWidth, INT& uiHeight)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		EDResourceNodeParser* pResourceNodeParser = GetRelativeResourceNodeParser();
		if (!pResourceNodeParser)
		{//没有绑定资源  给默认的宽高
			uiWidth  = DEFRESPNGWIDTH;
			uiHeight = DEFRESPNGHEIGHT;
			break;
		}

		pResourceNodeParser->GetImgSize(uiWidth, uiHeight);
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

EDResourceNodeParser* EDJSonParser::GetRelativeResourceNodeParser()
{
	return NULL;
}

double EDJSonParser::GetParserImageScale()
{
	return 1.0;
}

void EDJSonParser::SetParserItemRect(const CRect& rect)
{
	DMASSERT_EXPR(FALSE, L"SetParserItemRect not implement");
}

CPoint EDJSonParser::GetParserItemTopLeftPt()
{
	DMASSERT_EXPR(FALSE, L"GetParserItemTopLeftPt not implement");
	return std::move(CPoint());
}

int EDJSonParser::GetZPositionOrder()
{
	return -1;
}

bool EDJSonParser::SetZPositionOrder(int iZPostion)
{
	return false;
}

bool EDJSonParser::IsParserEnable()
{
	return true;
}

DMCode EDJSonParser::RemoveChildParser(EDJSonParser* pChild)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (this != pChild->GetParser(GPS_PARENT))// 被插入的子窗口的父窗口不是调用者！
		{
			break;
		}

		EDJSonParser *pPrevSib = pChild->GetParser(GPS_PREVSIBLING);
		EDJSonParser *pNextSib = pChild->GetParser(GPS_NEXTSIBLING);
		if (pPrevSib)
		{
			pPrevSib->m_Node.m_pNextSibling = pNextSib;
		}
		else
		{
			m_Node.m_pFirstChild = pNextSib;
		}
		if (pNextSib)
		{
			pNextSib->m_Node.m_pPrevSibling = pPrevSib;
		}
		else
		{
			m_Node.m_pLastChild = pPrevSib;
		}
		pChild->m_Node.m_pParent = NULL;
		pChild->m_Node.m_pOwner = NULL;
		pChild->m_Node.m_pFirstChild = pChild->m_Node.m_pLastChild = NULL;
		pChild->m_Node.m_pNextSibling = pChild->m_Node.m_pPrevSibling = NULL;
		m_Node.m_nChildrenCount--;
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDJSonParser::DestoryChildParser(EDJSonParser* pChild)
{
	if (this == pChild->GetParser(GPS_PARENT))// 被插入的子窗口的父窗口不是调用者！
	{
		pChild->DestoryParser();
		return DM_ECODE_OK;
	}
	return DM_ECODE_FAIL;
}

DMCode EDJSonParser::DestoryParser()
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		GetParserOwner()->JsonParserOnFreeNotify(this);
		EDJSonParser* pParentParser = GetParser(GPS_PARENT);
		if (pParentParser)// 先移除树形关系
		{
			pParentParser->RemoveChildParser(this);
		}

		EDJSonParser* pChildParser = GetParser(GPS_FIRSTCHILD);
		while (pChildParser)
		{
			EDJSonParser* pParserTmp = pChildParser;
			pChildParser = pChildParser->GetParser(GPS_NEXTSIBLING);
			pParserTmp->DestoryParser();
		}
		FreeParser(this);
	} while (false);
	return iErr;
}

void EDJSonParser::FreeParser(EDJSonParser* pParser)
{
	EDResourceNodeParser* pResourceNode = pParser->GetRelativeResourceNodeParser();
	if (pResourceNode)
	{
		pResourceNode->BindCurResourceParserOwner(NULL);//资源解除关联
	}
	delete pParser;
}

EDJSonParser* EDJSonParser::GetTopParentParser()
{
	EDJSonParser *pParent = this;
	while (pParent->GetParser(GPS_PARENT))
	{
		pParent = pParent->GetParser(GPS_PARENT);
	}

	return pParent;
}

EDJSonParser* EDJSonParser::FindChildParserByClassName(CStringW strClassName)
{
	EDJSonParser* pParser = GetParser(GPS_FIRSTCHILD);
	while (pParser)
	{
		if (_wcsicmp(pParser->V_GetClassName(), (LPCWSTR)strClassName) == 0)
			return pParser;
		EDJSonParser* pChildParser = pParser->FindChildParserByClassName(strClassName);
		if (pChildParser)
		{
			pParser = pChildParser;
			break;
		}
		pParser = pParser->GetParser(GPS_NEXTSIBLING);
	}
	return pParser;
}
