#include "stdAfx.h"
#include "DUIDragFrame.h"
#include "Helper.h"

DragMeta::DragMeta(int idx/*=-1*/,HCURSOR hCursor /*= ::LoadCursor(NULL,IDC_ARROW)*/)
{
	m_Index		 = idx;
	m_hCursor	 = hCursor;
	m_Clr		 = PBGRA(0,0,0xff,0xff);
	m_GrayClr	 = PBGRA(198,198,198,0xff);
	m_BlackClr	 = PBGRA(0, 0, 0, 0xff);
	m_bDragDot	 = true;
	m_bEnable	 = true;     
}

DMCode DragMeta::OnRender(IDMCanvas* pCanvas)
{
	if (0 == m_Index)
	{
		AutoDrawRoundRect(pCanvas, m_bEnable?m_Clr:m_GrayClr, PS_SOLID, 1, m_Rect, CPoint(0, 0));
	}
	else if (m_Index<9)
	{
		AutoFillRoundRect(pCanvas, m_bEnable ? m_Clr : m_GrayClr, m_Rect, CPoint(0, 0));
		AutoDrawRoundRect(pCanvas, m_bEnable ? m_BlackClr: m_GrayClr, PS_SOLID, 1, m_Rect, CPoint(0, 0));
	}
	else
	{
		if (m_pSkin)
		{
			m_pSkin->Draw(pCanvas,m_Rect,m_bEnable?0:1);
		}
	}

	return DM_ECODE_OK;
}

DMCode DragMeta::SetCurSor()
{
	if (m_bEnable)
	{
		::SetCursor(m_hCursor);
	}
	return DM_ECODE_OK;
}

// DUIDragFrame--------------------------------------
#define  DRAGSIZE    6
DUIDragFrame::DUIDragFrame()
{
	m_pCurdragMeta = NULL;
	m_pData = NULL;
	m_hHandCursor = NULL;
	m_dragMetaCount = 9;
	m_bMain = false;
	m_bDown = false;
	m_bKeyUp = true;
	m_pDUIXmlInfo->m_bFocusable = true;

	g_pDMApp->CreateRegObj((void**)&m_pStyle, NULL, DMREG_Style);
	DMASSERT(NULL != m_pStyle);
	DMXmlNode styleNode = g_pDMApp->GetStyle(L"myprivate:hand_cursor");
	m_pStyle->SetAttribute(L"cursor", styleNode.Attribute(L"cursor"), false);
	m_pStyle->GetCursor(m_hHandCursor);

	LinkDragMetaEvent();
}
 
DMCode DUIDragFrame::InitDragFrame(ObjTreeData* pData,CRect& rcLayout)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		if (NULL == pData)
		{
			break;
		}
		DUIWindow* &pDUIWnd = pData->m_pDUIWnd;
		DUIRoot* &pMainWnd = pData->m_pRootWnd;
		bool bMain = pDUIWnd == pMainWnd;// Root也算为Main
		bool bAllGray = false;
		if (!bMain)
		{
			if (pDUIWnd)
				if (!pDUIWnd->DM_IsVisible())
					bAllGray = true;
		}
		else
			break;

		CRect rcDrag = pDUIWnd->m_rcWindow;
		if (rcDrag.IsRectEmpty())
		{
			break;
		}
		InitDragMeta(rcDrag,bMain,bAllGray);

		// 计算自身frame的区域
		rcLayout = CalcDragFrameRect(rcDrag);

		m_pData = pData;
		m_bMain = bMain;
		m_pLayout = dynamic_cast<Layout*>(pData->m_pDUIWnd->m_pLayout.get());

		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

// private
void DUIDragFrame::LinkDragMetaEvent()
{
	m_dragMetas[0] = DragMeta(DSDOT_MOVE, m_hHandCursor ? m_hHandCursor : ::LoadCursor(NULL, IDC_SIZEALL));
	m_dragMetas[0].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragMove,this);

	m_dragMetas[1] = DragMeta(DSDOT_LEFT, ::LoadCursor(NULL, IDC_SIZEWE));
	m_dragMetas[1].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragLeft,this);

	m_dragMetas[2] = DragMeta(DSDOT_LEFTOP, ::LoadCursor(NULL, IDC_SIZENWSE));
	m_dragMetas[2].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragLeftTop,this);

	m_dragMetas[3] = DragMeta(DSDOT_TOP, ::LoadCursor(NULL, IDC_SIZENS));
	m_dragMetas[3].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragTop,this);

	m_dragMetas[4] = DragMeta(DSDOT_RIGHTOP, ::LoadCursor(NULL, IDC_SIZENESW));
	m_dragMetas[4].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragRightTop,this);

	m_dragMetas[5] = DragMeta(DSDOT_RIGHT, ::LoadCursor(NULL, IDC_SIZEWE));
	m_dragMetas[5].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragRight,this);

	m_dragMetas[6] = DragMeta(DSDOT_RIGHTBTM, ::LoadCursor(NULL, IDC_SIZENWSE));
	m_dragMetas[6].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragRightBottom,this);

	m_dragMetas[7] = DragMeta(DSDOT_BTM, ::LoadCursor(NULL, IDC_SIZENS));
	m_dragMetas[7].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragBottom,this);

	m_dragMetas[8] = DragMeta(DSDOT_LEFTBTM, ::LoadCursor(NULL, IDC_SIZENESW));
	m_dragMetas[8].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragLeftBottom,this);

	//m_dragMetas[9] = DragMeta(DSDOT_BOX, ::LoadCursor(NULL,IDC_HAND));
	//m_dragMetas[9].m_DragMetaAction = DMBind(&DUIDragFrame::OnDragBox,this);
}

void DUIDragFrame::InitDragMeta(CRect Rect,bool bMain,bool bAllGray)
{
	m_dragMetas[0].m_Rect = Rect; //DSDOT_MOVE
	Rect.InflateRect(DRAGSIZE, DRAGSIZE);

	//Left
	m_dragMetas[1].m_Rect = Rect; //DSDOT_LEFT
	m_dragMetas[1].m_Rect.top += (Rect.Height() - DRAGSIZE) / 2;
	m_dragMetas[1].m_Rect.bottom = m_dragMetas[1].m_Rect.top + DRAGSIZE;
	m_dragMetas[1].m_Rect.right = m_dragMetas[1].m_Rect.left + DRAGSIZE;
	m_dragMetas[1].m_Rect.SetRectEmpty(); //lzlong add 取消右边的点

	//LeftTop
	m_dragMetas[2].m_Rect = Rect; //DSDOT_LEFTOP
	m_dragMetas[2].m_Rect.bottom = Rect.top + DRAGSIZE;
	m_dragMetas[2].m_Rect.right = Rect.left + DRAGSIZE;
	
	//Top
	m_dragMetas[3].m_Rect = Rect; //DSDOT_TOP
	m_dragMetas[3].m_Rect.left += (Rect.Width() - DRAGSIZE) / 2;
	m_dragMetas[3].m_Rect.right = m_dragMetas[3].m_Rect.left + DRAGSIZE;
	m_dragMetas[3].m_Rect.bottom = Rect.top + DRAGSIZE;
	m_dragMetas[3].m_Rect.SetRectEmpty(); //lzlong add 取消上边的点

	//RightTop
	m_dragMetas[4].m_Rect = Rect; //DSDOT_RIGHTOP
	m_dragMetas[4].m_Rect.left = Rect.right - DRAGSIZE;
	m_dragMetas[4].m_Rect.bottom = m_dragMetas[4].m_Rect.top + DRAGSIZE;

	// Right
	m_dragMetas[5].m_Rect = Rect; //DSDOT_RIGHT
	m_dragMetas[5].m_Rect.top += (Rect.Height() - DRAGSIZE) / 2;
	m_dragMetas[5].m_Rect.bottom = m_dragMetas[5].m_Rect.top + DRAGSIZE;
	m_dragMetas[5].m_Rect.left = Rect.right - DRAGSIZE;
	m_dragMetas[5].m_Rect.SetRectEmpty(); //lzlong add 取消右边的点

	// RightBottom
	m_dragMetas[6].m_Rect = Rect; //DSDOT_RIGHTBTM
	m_dragMetas[6].m_Rect.top = Rect.bottom - DRAGSIZE;
	m_dragMetas[6].m_Rect.left = Rect.right - DRAGSIZE;

	// Bottom
	m_dragMetas[7].m_Rect = Rect; //DSDOT_BTM
	m_dragMetas[7].m_Rect.left += (Rect.Width() - DRAGSIZE) / 2;
	m_dragMetas[7].m_Rect.right = m_dragMetas[7].m_Rect.left + DRAGSIZE;
	m_dragMetas[7].m_Rect.top = Rect.bottom - DRAGSIZE;
	m_dragMetas[7].m_Rect.SetRectEmpty(); //lzlong add 取消下边的点

	// LeftBottom
	m_dragMetas[8].m_Rect = Rect; //DSDOT_LEFTBTM
	m_dragMetas[8].m_Rect.right = Rect.left + DRAGSIZE;
	m_dragMetas[8].m_Rect.top = Rect.bottom - DRAGSIZE;

	// DragMode
/*	m_dragMetas[9].m_Rect.right = m_dragMetas[2].m_Rect.left;
	m_dragMetas[9].m_Rect.bottom = m_dragMetas[2].m_Rect.top;
	m_dragMetas[9].m_Rect.left = m_dragMetas[2].m_Rect.left - 24;
	m_dragMetas[9].m_Rect.top = m_dragMetas[2].m_Rect.top - 24;
	m_dragMetas[9].m_Clr = PBGRA(0,0,0XFF,0XFF);
	m_dragMetas[9].m_pSkin = g_pDMApp->GetSkin(L"ds_drag_move");  */

	for (int i=0;i<m_dragMetaCount;i++)
	{
		m_dragMetas[i].m_bEnable = !bAllGray;
	}
}

void DUIDragFrame::UnlinkDragMeta()
{
	DM_ReleaseCapture();
	m_pLayout = NULL;
	m_pCurdragMeta = NULL;
	m_pData = NULL;
	m_bDown = false;
	m_StartDragPt = { 0, 0 };
	m_TrackDragPt = { 0, 0 };
	m_StartDragRc.SetRectEmpty();
	InitDragMeta(m_StartDragRc, false, false);
}

DragMeta* DUIDragFrame::HittestDragMeta(CPoint pt,bool bMain)
{
	int iCount = m_dragMetaCount;

	if (bMain)
	{
		iCount -= 1;// 9不显示了
	}

	for (int i = 1; i < iCount; ++i)
	{
		if (m_dragMetas[i].m_Rect.PtInRect(pt))
		{
			return &m_dragMetas[i];
		}
	}

	if (m_dragMetas[0].m_Rect.PtInRect(pt))
	{
		return &m_dragMetas[0];
	}

	return NULL;
}

void DUIDragFrame::OnDragLeft(DragMeta& meta, int iAction)
{
	if (meta.m_bEnable)
	{
		CRect rect;
		InternalHandleDrag(rect, (int*)&rect.left, NULL, meta.m_Index);
	}
}

void DUIDragFrame::OnDragLeftTop(DragMeta& meta, int iAction)
{
	if (meta.m_bEnable)
	{
		CRect rect;
		InternalHandleDrag(rect, (int*)&rect.left, (int*)&rect.top, meta.m_Index);
	}
}

void DUIDragFrame::OnDragTop(DragMeta& meta, int iAction)
{
	if (meta.m_bEnable)
	{
		CRect rect;
		InternalHandleDrag(rect, NULL, (int*)&rect.top, meta.m_Index);
	}
}

void DUIDragFrame::OnDragRightTop(DragMeta& meta, int iAction)
{
	if (meta.m_bEnable)
	{
		CRect rect;
		InternalHandleDrag(rect, (int*)&rect.right, (int*)&rect.top, meta.m_Index);
	}
}

void DUIDragFrame::OnDragRight(DragMeta& meta, int iAction)
{
	if (meta.m_bEnable)
	{
		CRect rect;
		InternalHandleDrag(rect, (int*)&rect.right, NULL, meta.m_Index);
	}
}

void DUIDragFrame::OnDragRightBottom(DragMeta& meta, int iAction)
{
	if (meta.m_bEnable)
	{
		CRect rect;
		InternalHandleDrag(rect, (int*)&rect.right, (int*)&rect.bottom, meta.m_Index);
	}
}

void DUIDragFrame::OnDragBottom(DragMeta& meta, int iAction)
{
	if (meta.m_bEnable)
	{
		CRect rect;
		InternalHandleDrag(rect, NULL, (int*)&rect.bottom, meta.m_Index);
	}
}

void DUIDragFrame::OnDragLeftBottom(DragMeta& meta, int iAction)
{
	if (meta.m_bEnable)
	{
		CRect rect;
		InternalHandleDrag(rect, (int*)&rect.left, (int*)&rect.bottom, meta.m_Index);
	}
}

void DUIDragFrame::OnDragBox(DragMeta& meta, int iAction)
{
	if (meta.m_bEnable)
	{
		CRect rect = m_StartDragRc; // rect总是表示当前大小
		int x = m_TrackDragPt.x - m_StartDragPt.x;
		int y = m_TrackDragPt.y - m_StartDragPt.y;
		rect.OffsetRect(x,y);
		SetElementRect(rect);

		m_pData->m_pRootWnd->AlignDragFrame(rect);//移动对齐参考线
	}
}

void DUIDragFrame::OnDragMove(DragMeta& meta, int iAction)
{
	OnDragBox(meta,iAction);
}

CRect DUIDragFrame::CalcDragFrameRect(CRect rcDrag)
{
	CRect rcLayout;
	rcLayout = rcDrag;
	rcLayout.InflateRect(DRAGSIZE,DRAGSIZE);
	return rcLayout;
}

// 背景绘背景图
BOOL DUIDragFrame::DM_OnEraseBkgnd(IDMCanvas* pCanvas)
{
	CRect rcClient;
	DV_GetClientRect(&rcClient);
	rcClient.DeflateRect(DRAGSIZE, DRAGSIZE, DRAGSIZE, DRAGSIZE);
	DMSmartPtrT<IDMSkin> pSkin;
	m_pDUIXmlInfo->m_pStyle->GetBgSkin(&pSkin);
	byte alpha = 0xff;
	m_pDUIXmlInfo->m_pStyle->GetAlpha(alpha);
	int iState = 0;
	DV_GetState(iState);
	if (pSkin.isNull())// 绘制颜色
	{
		DMColor ClrBg;
		m_pDUIXmlInfo->m_pStyle->GetBgColor(iState, ClrBg);
		if (!ClrBg.IsTextInvalid())
		{
			ClrBg.PreMultiply(alpha);
			pCanvas->FillSolidRect(&rcClient, ClrBg);
		}
	}
	else
	{
		int iNum = 0;
		pSkin->GetStates(iNum);
		if (iNum <= iState)
		{
			iState = 0;
		}
		pSkin->Draw(pCanvas, rcClient, iState, alpha);
	}
	return TRUE;
}

// public
void DUIDragFrame::DM_OnPaint(IDMCanvas* pCanvas)
{
	int i = 0/*1*/;
// 	if (m_bDown&&NULL != m_pCurdragMeta&&0 == m_pCurdragMeta->m_Index)
//  {
//  	i = 0;
//  }
	for (;i<9;i++)
	{
		m_dragMetas[i].OnRender(pCanvas);
	}
}

void DUIDragFrame::OnLButtonDown(UINT nFlags,CPoint pt)
{
	DragMeta* pMeta = HittestDragMeta(pt,false);
	if (pMeta)
	{
		//if (0 == pMeta->m_Index && m_pData)//lzlong add  外框有dragFrame覆盖的时候 选中上层的控件   使其选中
		//{
			//if (m_pData->m_pRootWnd->MLDownInSelMode(pt, m_pData->m_pDUIWnd))
			//	return;
		//}

		DV_SetFocusWnd();
		DM_SetCapture();
		m_bDown = true;
		pMeta->SetCurSor();
		m_StartDragPt = pt;
		m_TrackDragPt = m_StartDragPt;
		if (m_StartDragRc != m_dragMetas[0].m_Rect)
			m_StartDragRc = m_dragMetas[0].m_Rect;
		
	/*	if (0 == pMeta->m_Index && m_pData) //不需要了 
		{
			if (false == m_pData->m_pRootWnd->MLDownInSelMode(pt,m_pData->m_pDUIWnd))//说明点击在同一个dragframe上
			{
				DM_Invalidate();
			}
		}  */
	}
}

void DUIDragFrame::OnLButtonUp(UINT nFlags,CPoint pt)
{
	CRect EndDragRc = m_dragMetas[0].m_Rect;
	if (EndDragRc != m_StartDragRc)
	{
		DMASSERT(m_pData);
		if (m_pData)
		{
			INSERTNEWOBSERVEACTION(new EditorElemPosChgActionSlot(g_pMainWnd->m_pObjEditor->m_pObjTree->GetSelectedItem(), m_StartDragRc, EndDragRc, g_pMainWnd->m_pObjEditor->m_pObjTree, this));
		}
	}
	m_bDown = false;
	g_pMainWnd->m_pObjEditor->HideHoverFrameWnd();
	g_pMainWnd->m_pObjEditor->HideAlignMentFrameWnd();
	DM_ReleaseCapture();
	DM_Invalidate();
}

void DUIDragFrame::OnRButtonDown(UINT nFlags, CPoint pt)
{
	if (m_pData)
	{
		m_pData->m_pRootWnd->OnRButtonDown(nFlags,pt);
	}
}

void DUIDragFrame::OnMouseMove(UINT nFlags,CPoint pt)
{
	if (!m_bDown)
	{
		m_pCurdragMeta = HittestDragMeta(pt,false);
		if (m_pCurdragMeta && &m_dragMetas[0] != m_pCurdragMeta)
		{
			m_pCurdragMeta->SetCurSor();
		}
	}
	else if (m_bDown&&m_pCurdragMeta)
	{
		m_pCurdragMeta->SetCurSor();
		m_TrackDragPt = pt;
		if (m_TrackDragPt != m_StartDragPt)
		{
			m_pCurdragMeta->m_DragMetaAction(*m_pCurdragMeta,0);
		}
	}

	if (!g_pMainWnd->m_pObjEditor->m_pObjTree->m_hHoverItem) //lzlong add  为了右键菜单时  有hoveritem
		g_pMainWnd->m_pObjEditor->m_pObjTree->m_hHoverItem = g_pMainWnd->m_pObjEditor->m_pObjTree->m_hSelItem;
}

void DUIDragFrame::OnKeyDown(TCHAR nChar, UINT nRepCnt, UINT nFlags)
{
	CRect rtNew = m_dragMetas[0].m_Rect;
	if (rtNew.IsRectEmpty())
		return;
	if (VK_DOWN == nChar)
	{
		rtNew.OffsetRect(0, 5);
	}
	else if (VK_UP == nChar)
	{
		rtNew.OffsetRect(0, -5);
	}
	else if (VK_LEFT == nChar)
	{
		rtNew.OffsetRect(-5, 0);
	}
	else if (VK_RIGHT == nChar)
	{
		rtNew.OffsetRect(5, 0);
	}
	else if (VK_DELETE == nChar)
	{
		g_pMainWnd->HandleEffectTreeMenu(EFFECTTREEMENU_DELETE, g_pMainWnd->m_pObjEditor->m_pObjTree->GetSelectedItem());
		return;
	}
	else
		return;

	if (m_bKeyUp)
	{
		m_bKeyUp = false;
		m_rtStart = m_dragMetas[0].m_Rect;
	}

	SetElementRect(rtNew);
	SetMsgHandled(FALSE);
}

void DUIDragFrame::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (!m_bKeyUp)
	{
		m_bKeyUp = true;
		INSERTNEWOBSERVEACTION(new EditorElemPosChgActionSlot(g_pMainWnd->m_pObjEditor->m_pObjTree->GetSelectedItem(), m_rtStart, m_dragMetas[0].m_Rect, g_pMainWnd->m_pObjEditor->m_pObjTree, this));
	}
	SetMsgHandled(FALSE);
}

DUIWND DUIDragFrame::HitTestPoint(CPoint pt,bool bFindNoMsg)
{
	if (!m_bDown)
	{// 拖动过程时,保持m_pCurdragMeta不变
		DragMeta* pOldCurdragMeta = m_pCurdragMeta;
		m_pCurdragMeta = HittestDragMeta(pt,false);
		if (pOldCurdragMeta && !m_pCurdragMeta)//从hover在dragmeta到出边界
			g_pMainWnd->m_pObjEditor->HideHoverFrameWnd();
	}
	if (!m_pData)
		return NULL;

	if (NULL != m_pCurdragMeta&&0 == m_pCurdragMeta->m_Index)
	{
		m_pData->m_pRootWnd->HitTestPoint(pt,true);
	}
	else if (NULL == m_pCurdragMeta)
	{
		return m_pData->m_pRootWnd->HitTestPoint(pt,true);// 注意，这里使用了return,因为它不在dragframe的范围内了
	}
	if (m_pData && NULL != m_pCurdragMeta && 0 != m_pCurdragMeta->m_Index && m_pCurdragMeta->m_bEnable)
	{// 在DUIObjEditor保证了m_pDragFrame->DM_SetWndToTop(),从而HitTestPoint从dragframe->designeditor
		g_pMainWnd->m_pObjEditor->HoverInSelMode(m_pData->m_pDUIWnd);
	}
	
	return m_hDUIWnd;
}

void DUIDragFrame::InternalHandleDrag(CRect& rect, int* pHori, int* pVert, int dragType)
{
	do 
	{
		int x = (pHori ? (m_TrackDragPt.x - m_StartDragPt.x) : 0);
		int y = (pVert ? (m_TrackDragPt.y - m_StartDragPt.y) : 0);

		rect = m_StartDragRc; // rect总是表示当前大小
		if (pHori)
		{
			*pHori += x;
		}
		if (pVert)
		{
			*pVert += y;
		}
	
		DMASSERT(m_pData);
		if (m_pData && m_pData->m_pJsonParser) //等比缩放
		{
			INT uiWidth = 0;
			INT uiHeight = 0;
			m_pData->m_pJsonParser->GetRelativeResourceImgSize(uiWidth, uiHeight);
			if (pHori && pVert)
			{
				INT iNewWidth = 0;
				INT iNewHeight = 0;
				if (DSDOT_RIGHTBTM == dragType) //右下角拖拽
				{
					iNewWidth = *pHori - m_StartDragRc.left;
					iNewHeight = *pVert - m_StartDragRc.top;
					if (iNewWidth*uiHeight > iNewHeight*uiWidth && 0 != uiHeight)
					{
						iNewWidth = iNewHeight * uiWidth / uiHeight;
						*pHori = m_StartDragRc.left + iNewWidth;
					}
					else if (0 != uiWidth)
					{
						iNewHeight = iNewWidth * uiHeight / uiWidth;
						*pVert = m_StartDragRc.top + iNewHeight;
					}
				}
				else if (DSDOT_RIGHTOP == dragType)//右上角拖拽
				{
					iNewWidth = *pHori - m_StartDragRc.left;
					iNewHeight = m_StartDragRc.bottom - *pVert;
					if (iNewWidth*uiHeight > iNewHeight*uiWidth && 0 != uiHeight)
					{
						iNewWidth = iNewHeight * uiWidth / uiHeight;
						*pHori = m_StartDragRc.left + iNewWidth;
					}
					else if (0 != uiWidth)
					{
						iNewHeight = iNewWidth * uiHeight / uiWidth;
						*pVert = m_StartDragRc.bottom - iNewHeight;
					}
				}
				else if (DSDOT_LEFTOP == dragType) //左上角拖拽
				{
					iNewWidth = m_StartDragRc.right - *pHori;
					iNewHeight = m_StartDragRc.bottom - *pVert;

					if (iNewWidth * uiHeight > iNewHeight*uiWidth && 0 != uiHeight)
					{
						iNewWidth = iNewHeight * uiWidth / uiHeight;
						*pHori = m_StartDragRc.right - iNewWidth;
					}
					else if (0 != uiWidth)
					{
						iNewHeight = iNewWidth * uiHeight / uiWidth;
						*pVert = m_StartDragRc.bottom - iNewHeight;
					}
				}
				else if (DSDOT_LEFTBTM == dragType)//左下角拖拽
				{
					iNewWidth = m_StartDragRc.right - *pHori;
					iNewHeight = *pVert - m_StartDragRc.top;

					if (iNewWidth * uiHeight > iNewHeight*uiWidth && 0 != uiHeight)
					{
						iNewWidth = iNewHeight * uiWidth / uiHeight;
						*pHori = m_StartDragRc.right - iNewWidth;
					}
					else if (0 != uiWidth)
					{
						iNewHeight = iNewWidth * uiHeight / uiWidth;
						*pVert = m_StartDragRc.top + iNewHeight;
					}
				}
			}
		}

		if (!m_bMain)
		{
			rect.NormalizeRect();
			SetElementRect(rect);
			break;
		}

	} while (false);
}

bool DUIDragFrame::SetElementRect(CRect rect, bool bUpdateParserRect /*= true*/)
{
	bool bRet = false;
	do 
	{
		if (!g_pMainWnd || !m_pLayout) break;
		CRect rcNew = rect;
		// 关键是重设置4个POS_ITEM把rcOld转换成rcNew
		POS_ITEM PosLeft, PosTop, PosRight, PosBtm;
		memcpy(&PosLeft, &m_pLayout->m_Left, sizeof(POS_ITEM));
		memcpy(&PosTop, &m_pLayout->m_Top, sizeof(POS_ITEM));
		memcpy(&PosRight, &m_pLayout->m_Right, sizeof(POS_ITEM));
		memcpy(&PosBtm, &m_pLayout->m_Bottom, sizeof(POS_ITEM));
		bRet = m_pLayout->ParseItemByRect(rcNew, PosLeft, PosTop, PosRight, PosBtm);
		if (bUpdateParserRect)
			SetParserItemRect(PosLeft, PosTop, PosRight, PosBtm);

		g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pItem[0]->OnItemChange(&PosLeft);
		g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pItem[1]->OnItemChange(&PosTop);
		g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pItem[2]->OnItemChange(&PosRight);
		g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pItem[3]->OnItemChange(&PosBtm);
	} while (false);
	return bRet;
}

void DUIDragFrame::SetParserItemRect(POS_ITEM PosLeft, POS_ITEM PosTop, POS_ITEM PosRight, POS_ITEM PosBtm)
{
	if (g_pMainWnd->m_pObjEditor && g_pMainWnd->m_pObjEditor->m_pDragFrame && g_pMainWnd->m_pObjEditor->m_pDragFrame->m_pData && g_pMainWnd->m_pObjEditor->m_pDragFrame->m_pData->m_pJsonParser)
	{
		CRect rect((int)PosLeft.nPos, (int)PosTop.nPos, (int)PosRight.nPos, (int)PosBtm.nPos);
		rect.OffsetRect(-ROOTPNGXOFFSET, -ROOTPNGYOFFSET);
		g_pMainWnd->m_pObjEditor->m_pDragFrame->m_pData->m_pJsonParser->SetParserItemRect(rect); //实时设置数据
	}
}