// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DUIWndAutoMuteGuard.h
// File mark:   
// File summary:
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-6-13
// ----------------------------------------------------------------
#pragma once

class DUIWndAutoMuteGuard
{
public:
	DUIWndAutoMuteGuard(DUIWindow* pDuiWnd)
	{
		m_pDuiWnd = pDuiWnd;
		if (m_pDuiWnd)
		{
			MuteDUIWnd(m_pDuiWnd, true);
		}
	}

	~DUIWndAutoMuteGuard()
	{
		if (m_pDuiWnd)
		{
			MuteDUIWnd(m_pDuiWnd, false);
		}
	}
private:
	DUIWindow* m_pDuiWnd;
};