﻿// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EDDef.h
// File mark:   
// File summary:
// Author:		
// Edition:     1.0
// Create date: 2019-1-10
// ----------------------------------------------------------------
#pragma once

// ----------------
// 宏
#define  XML_ITEM									 L"item"
#define  XML_SEP									 L"sep"
#define  XML_URL									 L"url"
#define  XML_BDISABLE								 L"bdisable"
#define  XML_LAYOUT									 L"layout"
#define	 XML_BSHADOW								 L"bshadow"
#define  XML_ID										 L"id"
#define  XML_TEXT									 L"text"
#define  XML_SKIN									 L"skin"
#define  XML_PATH									 L"path"

#define  RESLIB_STATIC								 L"reslib_static"
#define  RESLIB_EDIT								 L"reslib_edit"
#define  RESLIB_SKIN								 L"reslib_skin"
#define  RESLIB_REDDOT								 L"reslib_reddot"
#define  DUINAME_EffectTreeCtrl						 L"EffectTreeCtrl"
#define  DUINAME_TransitionTreeCtrl					 L"TransitionTreeCtrl"
#define  DUINAME_REFERENCELINE						 L"ReferenceLine"
#define  DUINAME_PREVIEWPANEL						 L"DUIPreviewPanel"
#define  DUINAME_AlignmentFrame						 L"AlignmentFrame"
#define  DEFAULTWNDTITLE							 L"Effect Designer"
#define  EFFECTTREENAME								 L"EffectTree"
#define  DEFAULTPREVIEWSKIN							 L"PNG:ds_skinpreviewnull"
#define  REC_FILE                                    L".\\EDesigner.xml"
#define  CRASH_FILE                                  L".\\EDCrash.xml"

#define PRIVIEWSHRINKENLARGESCOPE		10
#define PRIVIEWSHRINKMINIMUM			30
#define PRIVIEWENLARGEMAXIMUM			500
// 因为wstring赋值空指针是会崩溃的 所以套个这个宏图个安心
#ifndef    SAFE_STR
#define    SAFE_STR(a) IsValidString(a) ? a :L""
#endif

#define MuteDUIWnd(pDuiWnd, bMute) pDuiWnd->m_EventMgr.SetMuted(bMute);

enum GlbMenuId
{
	GLBMENU_BASE = 10000,
	GLBMENU_NEW = 10000,
	GLBMENU_NEWEMPTYPROJ,
	GLBMENU_NEWPROJ,
	
	GLBMENU_OPEN,
	GLBMENU_CLOSE,
	GLBMENU_OPENRECENT,
	GLBMENU_SAVE,
	GLBMENU_EXPORTZIP,
	GLBMENU_SAVEAS,

	GLBMENU_BACKSTEP,
	GLBMENU_FORWORDSTEP,
	GLBMENU_SELECTALL,

	GLBMENU_CHINESELAN,
	GLBMENU_ENGLISHLAN,

	GLBMENU_ONLINEHELP,
	GLBMENU_CHECKUPDATE,
	GLBMENU_FEEDBACK,
};

enum ResLibListBoxMenuId
{
	RESLIBLISTBOX_BASE = 800,
	RESLIBLISTBOX_RENAME = 800,
	RESLIBLISTBOX_DELETE,
	RESLIBLISTBOX_MAX = 820,
};

enum EffectTreeMenuId
{
	EFFECTTREEMENU_BASE = 830,
	EFFECTTREEMENU_ADDCOMP = 830,//添加组件
	EFFECTTREEMENU_ADDEFFECT,
	EFFECTTREEMENU_DELEEFFECT,
	EFFECTTREEMENU_RENAME,
	EFFECTTREEMENU_UPITEM,
	EFFECTTREEMENU_DOWNITEM,
	EFFECTTREEMENU_DELETE,
	
 	EFFECTTREEMENU_EYESHADOW,//眼影
 	EFFECTTREEMENU_BLUSH,//腮红
 	EFFECTTREEMENU_EYE,//美瞳
 	EFFECTTREEMENU_LIPV2,//唇彩
 	EFFECTTREEMENU_NOSE,//鼻子
	EFFECTTREEMENU_FACETRANSPLANT,//换脸
//	EFFECTTREEMENU_2DSTICKER,//2D贴纸+

	EFFECTTREEMENU_ADD2DSTICKER,
	EFFECTTREEMENU_ADDMAKEUPS,
	EFFECTTREEMENU_ADDFACEEXCHANGE,
	EFFECTTREEMENU_ADDBACKGROUNDEDGE,
	EFFECTTREEMENU_ADDFACEDEFORMATION,
	EFFECTTREEMENU_ADDBEAUTIFY,
	EFFECTTREEMENU_IMPORT3DSTICKER,
	EFFECTTREEMENU_ADDTRANSITION,

	EFFECTTREEMENU_MAX = 859,
};

enum EditorElementMenuId
{
	EDITORELEMENT_BASE = 860,
	EDITORELEMENT_UPELEMENT = 860,
	EDITORELEMENT_DOWNELEMENT,
	EDITORELEMENT_SETUNVISIBLE,
	EDITORELEMENT_SETVISIBLE,
	EDITORELEMENT_IMPORTIMG,
	EDITORELEMENT_RESETPOSELEMENT,
	EDITORELEMENT_DELEELEMENT,
	EDITORELEMENT_MAX = 879,
};

enum ImpResLibBtnMenuId
{
	IMPRESLIBBTN_BASE = 880,
	IMPRESLIBBTN_IMPIMGFRAME = 880,//导入序列帧图片文件
	IMPRESLIBBTN_IMPGIFFILE,//导入gif文件
	IMPRESLIBBTN_IMPFROMFOLDER,//导入普通图片文件
	IMPRESLIBBTN_IMPMAKEUPIMG,//导入makeup图片
	IMPRESLIBBTN_MAX = 899,
};

enum EffectTempletType
{
	EFFECTTEMPBTN_BASE = 900,
	EFFECTTEMPBTN_2DEFFECT = 900,
	EFFECTTEMPBTN_2DSTICKERMAJOR,
	EFFECTTEMPBTN_FILTER,
	EFFECTTEMPBTN_MAKEUPV2,
	EFFECTTEMPBTN_HAIRCOLOR,
	EFFECTTEMPBTN_PORTRAITMAT,
	EFFECTTEMPBTN_3DEFFECT,
	EFFECTTEMPBTN_COMINGSOON,
	EFFECTTEMPBTN_NOSE,
	EFFECTTEMPBTN_BLUSH,
	EFFECTTEMPBTN_EYESHADOW,
	EFFECTTEMPBTN_EYE,
	EFFECTTEMPBTN_LIPV2,
	EFFECTTEMPBTN_FACETRANSPLANT,

	EFFECTTEMPBTN_STICKERSMAJOR,
	EFFECTTEMPBTN_TRANSITIONS,
	EFFECTTEMPBTN_TRANSITIONSNODE,
	EFFECTTEMPBTN_MAKEUPS,
	EFFECTTEMPBTN_MAKEUPNODE,
	EFFECTTEMPBTN_PARTS,
	EFFECTTEMPBTN_PARTNODE,
	EFFECTTEMPBTN_FACEEXCHANGE,
	EFFECTTEMPBTN_BACKGROUNDEDGE,
	EFFECTTEMPBTN_BEAUTIFY,
	EFFECTTEMPBTN_DEFORMATION,

	EFFECTTEMPBTN_2DSTIKERNODE,
	EFFECTTEMPBTN_MAX = 999,
};

enum
{
	GLBMENUBTN_ID_MIN = 100,
	GLBMENUBTN_ID_FILE,							///< 文件
	GLBMENUBTN_ID_EDIT,							///< 编辑
	GLBMENUBTN_ID_LANG,							///< language
	GLBMENUBTN_ID_HELP,							///< 帮助
	GLBMENUBTN_ID_MAX = 110
};

enum
{
	PARAMEINFOTAB_NULL = 0,
	PARAMEINFOTAB_TRANSITIONNODE,
	PARAMEINFOTAB_MAKEUPMAJOR,
	PARAMEINFOTAB_2DSTICKERNODE,
	PARAMEINFOTAB_MAKEUPNODE,
	PARAMEINFOTAB_RESLIBNODE,
	PARAMEINFOTAB_BACKGROUNDEDGE,
	PARAMEINFOTAB_BEAUTIFY,
	PARAMEINFOTAB_FACEEXCHANGE,
};

enum DesignMode { NoneMode = -1, FixedMode, SelectMode };
enum TriggerType
{
	NoneTrigger,	//无触发
	TriggerAppear,	//触发出现
	TriggerAppearWithAct,	//触发出现  动作结束立即消失
	TriggerDisappear,//触发消失
};

enum FaceTraceType
{
	FaceTraceAllFace,
	FaceTraceDefine,
};

// 结构体
struct GlbMenu
{
	int        id;
	CStringW   text;
};

struct StuPositionType
{
	CStringW   text;
	int        typeId;
	int        type2Id;
};

struct MainParser
{
	void* pMainWnd;
	void* pParser;
};

struct stuPoint
{
	double x;
	double y;
};

_declspec(selectany) stuPoint g_refPoints[] = \
{
	{ 152.097, 546.668 }, { 151.736, 576.369 }, { 152.229, 606.209 }, { 154.333, 635.982 }, { 157.772, 665.673 },
	{ 162.366, 695.013 }, { 168.29, 724.316 }, { 176.558, 752.952 }, { 188.226, 780.431 }, { 204.098, 805.64 },
	{ 223.816, 827.817 }, { 246.505, 847.369 }, { 271.275, 864.475 }, { 297.444, 879.4 }, { 324.989, 890.97 },
	{ 354.099, 897.99 }, { 383.876, 900.515 }, { 413.714, 898.016 }, { 443.084, 891.011 }, { 471.077, 879.624 },
	{ 497.755, 864.859 }, { 522.795, 847.981 }, { 545.665, 828.506 }, { 565.595, 806.221 }, { 581.623, 780.995 },
	{ 593.196, 753.628 }, { 601.472, 725.039 }, { 607.511, 695.768 }, { 612.08, 666.348 }, { 615.776, 636.857 },
	{ 617.927, 607.101 }, { 618.822, 577.328 }, { 618.822, 547.563 }, { 199.318, 489.186 }, { 228.797, 468.587 },
	{ 263.381, 459.629 }, { 300.037, 466.354 }, { 335.764, 476.272 }, { 436.755, 478.096 }, { 473.412, 467.237 },
	{ 510.891, 459.442 }, { 546.378, 467.72 }, { 576.335, 488.242 }, { 383.196, 539.661 }, { 382.87, 575.349 },
	{ 382.702, 611.406 }, { 382.515, 647.63 }, { 330.941, 687.674 }, { 356.967, 689.55 }, { 382.511, 692.196 },
	{ 408.231, 689.264 }, { 434.199, 687.398 }, { 233.697, 542.092 }, { 257.334, 524.396 }, { 305.281, 528.437 },
	{ 324.03, 551.485 }, { 298.943, 556.037 }, { 256.152, 553.648 }, { 446.307, 551.688 }, { 465.413, 528.667 },
	{ 513.385, 525.292 }, { 536.475, 543.703 }, { 513.808, 554.878 }, { 471.138, 556.467 }, { 233.195, 484.21 },
	{ 267.644, 482.709 }, { 301.969, 487.847 }, { 335.923, 494.447 }, { 437.041, 495.842 }, { 471.691, 488.19 },
	{ 506.768, 481.984 }, { 541.907, 482.999 }, { 281.699, 521.15 }, { 277.369, 557.1 }, { 278.099, 542.675 },
	{ 489.062, 521.591 }, { 492.549, 557.91 }, { 489.383, 543.394 }, { 352.932, 545.194 }, { 413.858, 545.405 },
	{ 333.597, 635.352 }, { 432.225, 635.199 }, { 315.825, 667.117 }, { 449.543, 666.717 }, { 297.406, 759.584 },
	{ 329.149, 748.428 }, { 360.638, 738.108 }, { 382.431, 741.105 }, { 403.866, 737.799 }, { 435.094, 747.178 },
	{ 466.455, 756.94 }, { 438.87, 778.959 }, { 405.931, 791.56 }, { 382.702, 793.317 }, { 359.502, 791.525 },
	{ 326.138, 779.68 }, { 308.886, 761.547 }, { 346.431, 760.409 }, { 382.596, 761.882 }, { 418.578, 759.641 },
	{ 455.52, 759.295 }, { 418.79, 760.842 }, { 382.634, 762.172 }, { 346.39, 761.389 }, { 278.285, 542.459 },
	{ 489.394, 543.221 }, { 0.00, 0.00 }, { 750.00, 0.00 }, { 0.00, 1334.00 }, { 750.00, 1334.00 }, {206.00, 950.00},
	{ 546.00, 950.00 }
};

_declspec(selectany) CPoint g_tracePoints[] = { { 74, 77 }, { 46, -1 }, { 43, -1 }, { 45, -1 }, { 84, 90 },
					{ 98, -1 }, { 101, 103 }, { 3, 29 }, { 11, 21 }, { 35, 40 }, { 110, 111 }, { 106, 107 } };

_declspec(selectany) StuPositionType g_PositionTypes[] = 
{
	{ L"Face - Position,Size,Depth,Rotation", 1, 0 },
	{ L"Face - Position,Size,Rotation", 2, 0 },
	{ L"Face - Position,Size", 4, 0 },
	{ L"Hand - OK", 512, 0 },
	{ L"Hand - Scissor", 1024, 0 },
	{ L"Hand - Thumb Up", 2048, 0 },
	{ L"Hand - Palm", 4096, 0 },
	{ L"Hand - Pistol", 8192, 0 },
	{ L"Hand - Love", 16384, 0 },
	{ L"Hand - Hold Up", 32768, 0 },
	{ L"Hand - Congratulate", 131072, 0 },
	{ L"Hand - Finger Heart", 262144, 0 },
	{ L"Hand - Index Finger", 1048576, 0 },
	{ L"Hand - 666", 4194304, 0 },
	{ L"Hand - BLESS", 8388608, 0 },
	{ L"Hand - ILOVEYOU", 0, 256 },
	{ L"Foreground", 0, 0 },
	{ L"Background", 65536, 0 },
};

_declspec(selectany) GlbMenu g_GlbMenuItem[] = \
{
	{ GLBMENU_NEW, L" 新建"},//0 
	{ GLBMENU_NEWEMPTYPROJ, L" 新建空项目" },//1 
	{ GLBMENU_NEWPROJ, L" 新建项目（Ctrl+N）" },//2 

	{ GLBMENU_OPEN, L" 打开工程（Ctrl+O）" },//3
	{ GLBMENU_CLOSE, L" 关闭工程（Alt+Q）" },//4
	{ GLBMENU_OPENRECENT, L" 打开最近" },//5
	{ GLBMENU_SAVE, L" 保存（Ctrl+S）" },//6
	{ GLBMENU_EXPORTZIP, L" 导出工程到Zip包" },//7
	{ GLBMENU_SAVEAS, L" 另存为（F12）" },//8

	{ GLBMENU_BACKSTEP, L" 后退一步（Ctrl+Z）" },//9
	{ GLBMENU_FORWORDSTEP, L" 前进一步（Ctrl+R）" },//10
	{ GLBMENU_SELECTALL, L" 全选" }, //11

	{ GLBMENU_CHINESELAN, L" 中文" },//12
	{ GLBMENU_ENGLISHLAN, L" English" },//13

	{ GLBMENU_ONLINEHELP, L" 在线教程" },//14
	{ GLBMENU_CHECKUPDATE, L" 检查更新" },//15
	{ GLBMENU_FEEDBACK, L" 问题反馈" }, //16
};

_declspec(selectany) GlbMenu g_ResMenuItem[] = \
{
	{ RESLIBLISTBOX_RENAME, L" 重命名"},//0 
	{ RESLIBLISTBOX_DELETE, L" 删除" },//1 
};

_declspec(selectany) GlbMenu g_EffectTempletBtnMenuItem[] = \
{
	{ EFFECTTEMPBTN_2DEFFECT, L"2D特效"},//0 
	{ EFFECTTEMPBTN_2DSTICKERMAJOR, L"2D贴纸"},//1 
	{ EFFECTTEMPBTN_FILTER, L"滤镜" },//2 
	{ EFFECTTEMPBTN_MAKEUPV2, L"美妆" },//3
	{ EFFECTTEMPBTN_HAIRCOLOR, L"染发" },//4
	{ EFFECTTEMPBTN_PORTRAITMAT, L"抠出人像" },//5
	{ EFFECTTEMPBTN_3DEFFECT, L"3D特效" },//6
	{ EFFECTTEMPBTN_COMINGSOON, L"敬请期待"}, //7
	{ EFFECTTEMPBTN_NOSE, L"鼻子" },//8
	{ EFFECTTEMPBTN_BLUSH, L"腮红" },//9
	{ EFFECTTEMPBTN_EYESHADOW, L"眼影" },//10
	{ EFFECTTEMPBTN_EYE, L"美瞳" },//11
	{ EFFECTTEMPBTN_LIPV2, L"唇彩" },//12
	{ EFFECTTEMPBTN_FACETRANSPLANT, L"换脸"},//13

	{ EFFECTTEMPBTN_STICKERSMAJOR, L"Stickers" },//14
	{ EFFECTTEMPBTN_TRANSITIONS, L"Transitions" },//15
};

_declspec(selectany) GlbMenu g_EffectTreeMenuItem[] = \
{
	{ EFFECTTREEMENU_ADDCOMP, L"添加组件"},//0 
	{ EFFECTTREEMENU_ADDEFFECT, L"添加特效" },//1
	{ EFFECTTREEMENU_DELEEFFECT, L"删除特效" },//2 
	{ EFFECTTREEMENU_RENAME, L"重命名" },//3
	{ EFFECTTREEMENU_UPITEM, L"上移一项" },//4
	{ EFFECTTREEMENU_DOWNITEM, L"下移一项" },//5
	{ EFFECTTREEMENU_DELETE, L"删除" },//6
	
	{ EFFECTTREEMENU_EYESHADOW, L"眼影 - Eye Shadow" },//7
	{ EFFECTTREEMENU_BLUSH, L"腮红 - Blush" },//8
	{ EFFECTTREEMENU_EYE, L"美瞳 - Eye" }, //9
	{ EFFECTTREEMENU_LIPV2, L"唇彩 - Lip" },//10
	{ EFFECTTREEMENU_NOSE, L"鼻子 - Nose" },//11
	{ EFFECTTREEMENU_FACETRANSPLANT, L"换脸 - None" },//12

	{ EFFECTTREEMENU_ADD2DSTICKER, L"2D贴纸 - 2DSticker" },//13
	{ EFFECTTREEMENU_ADDMAKEUPS, L"美妆 - Makeups" },//14
	{ EFFECTTREEMENU_ADDFACEEXCHANGE, L"换脸 - Face Exchange" },//15
	{ EFFECTTREEMENU_ADDBACKGROUNDEDGE, L"Background Edge" },//16
	{ EFFECTTREEMENU_ADDFACEDEFORMATION, L"Face Deformation" },//17
	{ EFFECTTREEMENU_ADDBEAUTIFY, L"美化 - Beautify" },//18
	{ EFFECTTREEMENU_IMPORT3DSTICKER, L"Import 3D Sticker" },//19
	{ EFFECTTREEMENU_ADDTRANSITION, L"Add Transition" },//20
};

_declspec(selectany) GlbMenu g_EditorElementMenuItem[] = \
{
	{ EDITORELEMENT_UPELEMENT, L"上移一层"},//0 
	{ EDITORELEMENT_DOWNELEMENT, L"下移一层" },//1
	{ EDITORELEMENT_SETUNVISIBLE, L"隐藏" }, //2
	{ EDITORELEMENT_SETVISIBLE, L"显示" }, //3
	{ EDITORELEMENT_IMPORTIMG, L"导入图片" }, //4
	{ EDITORELEMENT_RESETPOSELEMENT, L"初始宽高" },//5
	{ EDITORELEMENT_DELEELEMENT, L"删除" },//5
};

_declspec(selectany) GlbMenu g_ImpResLibBtnMenuItem[] = \
{
	{ IMPRESLIBBTN_IMPIMGFRAME, L"导入图片/序列帧"},//0 
	{ IMPRESLIBBTN_IMPGIFFILE,  L"导入GIF文件"},//1
	{ IMPRESLIBBTN_IMPFROMFOLDER, L"文件夹批量导入"},//2
};

enum TransitionMenuId
{
	TransitionMenu_BASE = 200,
	TransitionMenu_AddTransition = 200,
	TransitionMenu_RenameTransition,
	TransitionMenu_DeleTransition,
	TransitionMenu_AddCondition,
	TransitionMenu_AddTarget,
	TransitionMenu_DeleCondition,
	TransitionMenu_AddEvent,
	TransitionMenu_DeleEvent,
	TransitionMenu_DeleTarget,

	TransitionMenu_ShowTransition,

	TransitionMenu_MAX = 230,
};

_declspec(selectany) GlbMenu g_TransitionMenuItem[] = \
{
	{ TransitionMenu_AddTransition, L"Add Transition"},//0 
	{ TransitionMenu_RenameTransition, L"Rename" },//1 
	{ TransitionMenu_DeleTransition, L"Delete Transition" },//2 
	{ TransitionMenu_AddCondition, L"Add Condition" },//3
	{ TransitionMenu_AddTarget, L"Add Target" },//4
	{ TransitionMenu_DeleCondition, L"Delete Condition" },//5
	{ TransitionMenu_AddEvent, L"Add Event" },//6
	{ TransitionMenu_DeleEvent, L"Delete Event" },//7
	{ TransitionMenu_DeleTarget, L"Delete Target" },//8
};

#define  MAKEUPDIR L"\\makeupres\\"
enum MakeupResId
{
	MakeupRes_EyeShadowA,
	MakeupRes_EyeShadowB,
	MakeupRes_EyeShadowC,
	MakeupRes_EyeShadowD,
	MakeupRes_EyeShadowE,
	MakeupRes_EyeShadowF,
	MakeupRes_EyeShadowG,
	MakeupRes_BlushA,
	MakeupRes_BlushB,
	MakeupRes_EyeA,
	MakeupRes_EyeB,
	MakeupRes_EyeC,
	MakeupRes_EyeD,
	MakeupRes_LipA,
	MakeupRes_LipB,
	MakeupRes_NoseA,
	MakeupRes_NoseB,
	MakeupRes_FaceTransplantA,
};

_declspec(selectany) GlbMenu g_MakeupResItems[] = \
{
	{ MakeupRes_EyeShadowA, L"EyeShadowA.png"},//0 
	{ MakeupRes_EyeShadowB, L"EyeShadowB.png" },//1 
	{ MakeupRes_EyeShadowC, L"EyeShadowC.png" },//2 
	{ MakeupRes_EyeShadowD, L"EyeShadowD.png" },//3
	{ MakeupRes_EyeShadowE, L"EyeShadowE.png" },//4
	{ MakeupRes_EyeShadowF, L"EyeShadowF.png" },//5
	{ MakeupRes_EyeShadowG, L"EyeShadowG.png" },//6
	{ MakeupRes_BlushA, L"BlushA.png" },//7
	{ MakeupRes_BlushB, L"BlushB.png" },//8
	{ MakeupRes_EyeA, L"EyeA.png"},//9
	{ MakeupRes_EyeB, L"EyeB.png" },//10
	{ MakeupRes_EyeC, L"EyeC.png" },//11
	{ MakeupRes_EyeD, L"EyeD.png" },//12
	{ MakeupRes_LipA, L"LipA.png" },//13
	{ MakeupRes_LipB, L"LipB.png" },//14
	{ MakeupRes_NoseA, L"NoseA.png" },//15
	{ MakeupRes_NoseB, L"NoseB.png" },//16
	{ MakeupRes_FaceTransplantA, L"FaceTransplantA.png" },//17
};
//Transition树节点节点类型
enum TagTransitionTree
{
	TagTransitionNULL,
	TagTransitionMajor,//Transition 顶层节点
	TagTransitionNode, //Transition 子节点
	TagConditionNode,  //Condition 子节点
	TagConditionEventNode, //Event 子节点
	TagTargetNode, //Target 子节点
	TagRandomShowNode, //RandomShow 子节点
};