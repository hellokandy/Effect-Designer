// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	Helper.h
// File mark:   
// File summary:
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-3-1
// ----------------------------------------------------------------
#pragma once

bool IsDirectoryExist(CStringW strDir);///< 判断路径是否为文件夹
bool IsFileExist(CStringW strFilePath);///< 判断路径是否为文件
bool RemoveDir(LPCWSTR szFileDir);	   ///< 删除文件夹
bool CopyDir(LPCWSTR szFileSrcDir, LPCWSTR szFileDestDir);	   ///< 拷贝文件夹
CStringW GetAppdataPath();
bool OpenFolderAndSelectFile(CStringW strFilePath);///< 如果文件夹已经打开，就不用再重新打开一次，直接置顶显示，并选中目标文件http://www.cnblogs.com/shankun/p/4088860.html
bool CopyDirectory(CStringW strSrcDir,CStringW strDestDir);///< 复制文件夹，注意，如果目标文件夹已存在，则在源文件夹为名复制到目标文件夹的子目录下
CStringW GetProgramDirectory();
BOOL SelectFolderPath(CStringW& strFilePath);	///< 选择文件夹
BOOL SelectFolderPathEx(CStringW& strFilePath);	///< 改进后的选择文件夹
bool DlgImportImages(CStringW& filePath, CArray<CStringW>& images);

int StringToInt(CStringW str);
CStringW IntToString(int id);
CStringW FloatToString(float id);
CStringW DoubleToString(double id);

// 绘制函数二次封装
DMCode AutoDrawText(IDMCanvas*pCanvas,CStringW strFont,DMColor TextClr,LPCWSTR lpString, int nCount, LPRECT lpRect, UINT uFormat,BYTE alpha=0xFF);
DMCode AutoDrawRoundRect(IDMCanvas*pCanvas,DMColor TextClr,int iStyle,int iWidth,LPCRECT lpRect,POINT &pt);
DMCode AutoFillRoundRect(IDMCanvas*pCanvas,DMColor BrushClr,LPCRECT lpRect,POINT &pt);
DMCode AutoDrawDotLine(IDMCanvas*pCanvas, DMColor TextClr, int iStyle, int iWidth, LPPOINT lpPt, int cPoints);

DMCode ListDirectory(const std::wstring& strFilePath, std::function<bool(const std::wstring&, const std::wstring&, bool)> callBack, bool recursie = false);

// EDesigner.xml记录了最近打开的项目
class DMCtrlXml
{
public:
	DMCtrlXml();
	~DMCtrlXml();
	
	bool AddRecentResDir(CStringW strNewDir);
	bool RemoveRecentResDir(CStringW strNewDir);

	bool AddCrashBakProj(const CStringW& strOrgPath, const CStringW& strBackUpPath);
	void GetCrashBakProj(CStringW& strOrgPath, CStringW& strBackUpPath);
public:
	//crash
	DMXmlDocument	m_CrashDoc;
	// recent
	DMXmlDocument   m_RecentDoc;
};