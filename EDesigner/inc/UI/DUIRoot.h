// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DUIRoot.h 
// File mark:   
// File summary:editor绘制区域的底层窗口，其他子窗口在这个窗口里面。
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-2-28
// ----------------------------------------------------------------
#pragma once
#include <memory>

class ObjTreeData;
class DUIObjEditor;

namespace ED
{
	class DUIEffectTreeCtrl;
}

namespace DM
{
	class PosEdit;
}

//定义人物在图片中的位置偏移
/*
#define ROOTPNGXOFFSET	540
#define ROOTPNGYOFFSET	480
#define ROOTWNDYOFFSET	10
#define ROOTPNGSIZEX	540
#define ROOTPNGSIZEY	960
*/
#define ROOTPNGXOFFSET	750
#define ROOTPNGYOFFSET	667
#define ROOTWNDYOFFSET	10
#define ROOTPNGSIZEX	750
#define ROOTPNGSIZEY	1334
#define PNGWHOLESIZEX	2250
#define PNGWHOLESIZEY	2668

class DUIRoot:public DUIWindow
{
	DMDECLARE_CLASS_NAME(DUIRoot, L"Root", DMREG_Window)
public:
	DUIRoot();

	DMCode InitDesignEditor(HDMTREEITEM hRootTree);
	DMCode SetDesignMode(DesignMode ds_mode);

public:
	DM_BEGIN_MSG_MAP()
		MSG_WM_LBUTTONDOWN(OnLButtonDown)
		MSG_WM_LBUTTONUP(OnLButtonUp)
		MSG_WM_RBUTTONDOWN(OnRButtonDown)
	DM_END_MSG_MAP()
	void OnLButtonDown(UINT nFlags,CPoint pt);
	void OnLButtonUp(UINT nFlags,CPoint pt);
	void OnRButtonDown(UINT nFlags, CPoint pt);

	DUIWND HitTestPoint(CPoint pt,bool bFindNoMsg);///< 过滤消息分发
	DMCode AlignDragFrame(const CRect& rtWnd);		///< 对齐参考线

	// 模式处理
	HDMTREEITEM SelOrHoverTreeItemByDUIWnd(HDMTREEITEM hRoot,DUIWindow* pDUIWnd,bool bSel = false);
	bool MLDownInSelMode(CPoint pt,DUIWindow* pCurSelWnd);

public:
	// AddMode下的操作
	static DUIWindow* CreateAddChild(DUIWindow* pParentWnd,CStringW strReg);							///< 创建子控件
	bool InitAddChild(ObjTreeData* pParentData, DUIWindow* pWnd, EDJSonParserPtr pJSonParser);	///< 初始化子控件
	DUIWindow* GetAddChild(HDMTREEITEM parent, EDJSonParserPtr pJSonParser);

public:
	DM_BEGIN_ATTRIBUTES()
		DM_CHAIN_ATTRIBUTTE(m_pDUIXmlInfo)
	DM_END_ATTRIBUTES()
	DMCode OnAttributeFinished(LPCWSTR pszAttribute,LPCWSTR pszValue,bool bLoadXml,DMCode iErr);

	DUIObjEditor*										m_pParent;
	ED::DUIEffectTreeCtrl*								m_pObjTree;
	DesignMode											m_DesignMod;
	HDMTREEITEM											m_hRoot;
	DUIWindow*											m_pCurSeleDUIWnd;

	// 拖动操作
	bool												m_bDown;							///<鼠标按下
	CPoint												m_StartDragPt;
	CPoint												m_TrackDragPt;
	CRect												m_StartDragRc;
	int													m_szWndInitSizeX;

	// Add操作
	DUIWindow*											m_pPreHoverWnd;
	static CPoint                                       m_pAddParentPt;
};