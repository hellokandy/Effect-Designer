// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DUIPos.h
// File mark:   
// File summary:模似pos的锚点布局坐标系
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-3-8
// ----------------------------------------------------------------
#pragma once

namespace DM
{
	enum EDITPOS
	{
		EDITPOS_NULL,
		EDITPOS_LEFT,
		EDITPOS_TOP,
		EDITPOS_RIGHT,
		EDITPOS_BOTTOM,
	};
	/// <summary>
	///		edit,加入滚动变化效果
	/// </summary>
	class PosItem;
	class PosEdit : public DUIEdit
	{
		DMDECLARE_CLASS_NAME(PosEdit, L"posedit", DMREG_Window);
	public:
		DM_BEGIN_MSG_MAP()
			MSG_WM_CHAR(OnChar)
			MESSAGE_HANDLER_EX(WM_IME_COMPOSITION, OnImeChar)
			MSG_WM_MOUSEWHEEL(OnMouseWheel)
		DM_END_MSG_MAP()
	public:
		PosEdit();
		DMCode DV_CreateChildWnds(DMXmlNode &XmlNode);
		DMCode OnEditChange(DMEventArgs *pEvt);
		void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
		LRESULT OnImeChar(UINT uMsg, WPARAM wParam, LPARAM lParam);
		BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
		
		PosItem*	m_pPosItem;
		EDITPOS		m_editPostype;
	};

	/// <summary>
	///		模拟POS_ITEM 
	/// </summary>   
	class PosItem : public DMBase
	{
		DMDECLARE_CLASS_NAME(PosItem,L"positem",DMREG_Window);
	public:
		PosItem();

		DMCode SetEditItem(PosEdit* pEdit);
		DMCode InitPosItem(POS_ITEM* pItem, Layout* pLayout, PosEdit* pEdit);
		DMCode UnInitPosItem();
		
		DMCode OnItemChange(POS_ITEM* pItem = NULL);

	public:
		DMSmartPtrT<PosEdit>             m_pEdit;
		POS_ITEM*                        m_pItem;
		DMSmartPtrT<Layout>				 m_pLayout;///< 指向要编辑的layout
		ED::DUIEffectTreeCtrl*			 m_pObjTreeCtrl;
	};

	/// <summary>   
	///		模拟锚点pos
	/// </summary>
	class DUIPos : public DMBase
	{
		DMDECLARE_CLASS_NAME(DUIPos,L"duipos",DMREG_Window);
	public:
		DUIPos();
		DMCode InitLayout(IDMLayout* pLayout, PosEdit** ppPosEdit);
		DMCode UnInitLayout();

	public:
		DMSmartPtrT<PosItem>                    m_pItem[4];
		DMSmartPtrT<Layout>						m_pLayout;///< 指向要编辑的layout

		DMSmartPtrT<DM::PosEdit>				m_pElementSWHEditItem[3];// 位置信息的缩放比 宽度 高度
	};
}