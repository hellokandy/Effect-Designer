// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DUISplitLayoutEx.h
// File mark:   
// File summary:splitlayout的扩展  支持隐藏一个分区
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-7-18
// ----------------------------------------------------------------

#pragma once

namespace DMAttr
{
	/// <summary>
	///		<see cref="DM::DUISplitLayoutEx"/>的xml属性定义
	/// </summary>
	class DUISplitLayoutExAttr :public DUIFlowLayoutAttr
	{
	public:
		static wchar_t* bool_bhideonepartition;                                    ///< 是否隐藏一个分区,示例:bhideonepartition="1"
	};
	DMAttrValueInit(DUISplitLayoutExAttr, bool_bhideonepartition)
}

namespace DM
{
	class DUISplitLayoutEx : public DUISplitLayout
	{
		DMDECLARE_CLASS_NAME(DUISplitLayoutEx, L"splitlayoutEx", DMREG_FlowLayout)

	public:
		DUISplitLayoutEx();
		~DUISplitLayoutEx();

		DM_BEGIN_MSG_MAP()
			DM_MSG_WM_PAINT(DM_OnPaint)
		DM_END_MSG_MAP()

		DMCode DV_UpdateChildLayout();
		void DM_OnPaint(IDMCanvas* pCanvas);

		DM_BEGIN_ATTRIBUTES()
			DM_bool_ATTRIBUTE(DMAttr::DUISplitLayoutExAttr::bool_bhideonepartition, m_bHideOnePartition, DM_ECODE_NOXMLRELAYOUT)
		DM_END_ATTRIBUTES()

		DMCode OnAttributeFirstChildWidth(LPCWSTR lpszValue, bool bLoadXml);

	public:
		bool m_bHideOnePartition;
	};
}


